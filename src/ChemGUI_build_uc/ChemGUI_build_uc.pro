TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$BINDIR

include(../ChemGUI_parse_lib.pri)

SOURCES += main.cpp \
    generate_uc_atoms.cpp

HEADERS += \
    generate_uc_atoms.h
