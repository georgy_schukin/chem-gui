#include "chemgui_parse.h"
#include "parse/structure_util.h"

#include <iostream>
#include <memory>

int main(int argc, char *argv[]) {
    try {
        std::unique_ptr<StructureData> dt(new StructureData());
        StructureData *data = dt.get();
        parse_shelx("../data/test.ins", data);
        // update(data)
        // write_xyz(data)
        std::cout << *(data) << std::endl;
    }
    catch (const std::exception &e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }
}
