TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS = ChemGUI_lib \
    ChemGUI_parse_lib \
    ChemGUI_wrap_lib \
    ChemGUI_app \
    ChemGUI_gui \    
    ChemGUI_parse_test \
    ChemGUI_parse_exec \
    ChemGUI_build_uc \    
    ChemGUI_exec \    
    ChemGUI_exec_c

#ChemGUI_app.depends = ChemGUI_lib
#ChemGUI_gui.depends = ChemGUI_lib
#ChemGUI_wrap_lib.depends = ChemGUI_lib
#ChemGUI_parse_test.depends = ChemGUI_parse_lib
#ChemGUI_parse_exec.depends = ChemGUI_parse_lib
#ChemGUI_build_uc.depends = ChemGUI_parse_lib
#ChemGUI_exec.depends = ChemGUI_wrap_lib ChemGUI_lib
