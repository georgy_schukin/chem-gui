TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$BINDIR
TARGET = bvscalc_c

include(../ChemGUI_lib.pri)

SOURCES += main.c
