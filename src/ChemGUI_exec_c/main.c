#include "chemgui.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct IonData *ion;
struct MapData *map;
struct MapPosData *pos;
struct BVSData *bvs;

enum CubType {
    BVS,
    DA,
    GII
};

void printUsage(char **argv) {
    printf("Usage: %s input file <command>\n", argv[0]);
    printf("Command:\n");
    printf("\tinfo\n");
    printf("\tcalc: ion_num grid_size output_dir\n");
}

const char* getIonLabel(int num) {
    return ion->ions[num].ion_label;
}

void printInfo(const char *input_file) {
    int i;
    read_data_from_file(input_file, ion, map, pos, 0);
    for (i = 0; i < ion->total_ion_num.both; i++) {
        printf("%s\n", getIonLabel(i));
    }
    printf("\n");
    printf("%f\n", get_min_grid_size(map));
}

int get_output_filename(enum CubType type, int ion_num, const char *output_dir, char *filename) {
    char output_file[256];
    switch (type) {
    case BVS:
        get_cub_filename("bvs", "", getIonLabel(ion_num), output_file);
        break;
    case DA:
        get_cub_filename("da", "", getIonLabel(ion_num), output_file);
        break;
    case GII:
        get_cub_filename("gii", "", getIonLabel(ion_num), output_file);
        break;
    default:
        return -1;
    }
    if (strlen(output_dir) > 0) {
        sprintf(filename, "%s/%s", output_dir, output_file);
    } else {
        sprintf(filename, "%s", output_file);
    }
    return 0;
}

void output(enum CubType type, int ion_num, const char *output_dir) {
    char filename[1024];
    if (get_output_filename(type, ion_num, output_dir, filename) == -1) {
        return;
    }
    switch (type) {
    case BVS:
        save_bvs_cube(filename, ion_num, ion, bvs, map, pos);
        break;
    case DA:
        save_da_cube(filename, ion_num, ion, bvs, map, pos);
        break;
    case GII:
        save_gii_cube(filename, ion_num, ion, bvs, map, pos);
        break;
    default:
        return;
    }
    printf("%s\n", filename);
}


void calc(const char *input_file, int ion_num, double grid_size, const char *output_dir) {
    read_data_from_file(input_file, ion, map, pos, 0);
    set_grid_size(map, grid_size);
    clear_bvs(bvs);
    calc_bvs(ion_num, ion, bvs, map, pos, 0);
    output(BVS, ion_num, output_dir);
    output(DA, ion_num, output_dir);
    output(GII, ion_num, output_dir);
}

int main(int argc, char **argv) {
    if (argc <= 2) {
        printUsage(argv);
        return -1;
    }

    ion = (struct IonData*)malloc(sizeof(struct IonData));
    bvs = (struct BVSData*)malloc(sizeof(struct BVSData));
    map = (struct MapData*)malloc(sizeof(struct MapData));
    pos = (struct MapPosData*)malloc(sizeof(struct MapPosData));

    const char *input_file = argv[1];
    const char *command = argv[2];

    if (strcmp(command, "info") == 0) {
        printInfo(input_file);
    } else if (strcmp(command, "calc") == 0) {
        const int ion_num = atoi(argv[3]);
        const double grid_size = atof(argv[4]);
        const char *output_dir = argc >= 6 ? argv[5] : "";
        calc(input_file, ion_num, grid_size, output_dir);
    } else {
        printUsage(argv);
        return -1;
    }

    free(ion);
    free(bvs);
    free(map);
    free(pos);

    return 0;
}
