#include "chemgui_wrap.h"
#include "chemgui.h"

namespace chemgui {

ChemGUIWrap::ChemGUIWrap() {
    init();
}

ChemGUIWrap::ChemGUIWrap(const std::string &filename) {
    init();
    load(filename);
}

ChemGUIWrap::~ChemGUIWrap() {
}

void ChemGUIWrap::init() {
    ion.reset(new IonData());
    bvs.reset(new BVSData());
    map.reset(new MapData());
    pos.reset(new MapPosData());
}

void ChemGUIWrap::load(const std::string &filename) {
    read_data_from_file(filename.c_str(), ion.get(), map.get(), pos.get(), 0);
}

int ChemGUIWrap::getNumOfIons() const {
    return ion->total_ion_num.both;
}

std::string ChemGUIWrap::getIonType(int num) const {
    return std::string(ion->ions[num].ion_label);
}

void ChemGUIWrap::setGridSize(double grid_size) {
    set_grid_size(map.get(), static_cast<float>(grid_size));
}

double ChemGUIWrap::getMinGridSize() const {
    return get_min_grid_size(map.get());
}

void ChemGUIWrap::calculateBVS(int ion_type) {
    clear_bvs(bvs.get());
    calc_bvs(ion_type, ion.get(), bvs.get(), map.get(), pos.get(), 0);  // Calculate BVS for i-th ion type
}

std::string ChemGUIWrap::saveBVS(int ion_type, const std::string &output_dir) {
    const std::string ion_label = getIonType(ion_type);
    const std::string bvs_filename = output_dir + "/" + ion_label + ".cub";
    //const std::string gii_filename = output_dir + "/GII_" + ion_label + ".cub";
    //const std::string da_filename = output_dir + "/DA_" + ion_label + ".cub";
    save_bvs_cube(bvs_filename.c_str(), ion_type, ion.get(), bvs.get(), map.get(), pos.get());
    //save_gii_cube(gii_filename.c_str(), ion_type, ion.get(), bvs.get(), map.get(), pos.get());
    //save_da_cube(da_filename.c_str(), ion_type, ion.get(), bvs.get(), map.get(), pos.get());
    return bvs_filename;
}

}



