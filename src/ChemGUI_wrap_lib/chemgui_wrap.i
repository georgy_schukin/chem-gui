%module chemgui_wrap

%{
#include "chemgui_wrap.h"
%}

%include "std_string.i"

class ChemGUIWrap {
public:
    ChemGUIWrap();
    ~ChemGUIWrap();

    void load(const std::string &filename);

    int getNumOfIons() const;
    std::string getIonType(int num) const;

    void setGridSize(double grid_size);

    void calculateBVS(int ion_type);

    void saveBVS(int ion_type, const std::string &output_dir);
};
