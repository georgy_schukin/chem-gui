#-------------------------------------------------
#
# Project created by QtCreator 2016-10-10T13:49:56
#
#-------------------------------------------------

QT       -= core gui

TEMPLATE = lib
CONFIG += staticlib c++11

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$LIBDIR
TARGET = $$WRAP_LIB

#DEFINES += WRAP_LIBRARY

include(../ChemGUI_lib.pri)

#swig.target = wrap.cpp
#swig.commands = swig -c++ -java -package org.ssd.chemgui.wrap -o $$swig.target $$PWD/chemgui_wrap.i

#QMAKE_EXTRA_TARGETS += swig
#QMAKE_CLEAN += $$swig.target

#this.depends = swig

unix {
    INCLUDEPATH += \"${JAVA_HOME}/include\" \"${JAVA_HOME}/include/linux\"
}

win32 {
    INCLUDEPATH += \"${JAVA_HOME}/include\" \"${JAVA_HOME}/include/win32\"
}

SOURCES += \
    chemgui_wrap.cpp

HEADERS += \
    chemgui_wrap.h

#OTHER_FILES += chemgui_wrap.i

unix {
    target.path = /usr/lib
    INSTALLS += target
}
