#pragma once

#include <memory>
#include <string>

struct IonData;
struct BVSData;
struct MapData;
struct MapPosData;

namespace chemgui {

class ChemGUIWrap {
public:
    ChemGUIWrap();
    ChemGUIWrap(const std::string &filename);
    ~ChemGUIWrap();

    void load(const std::string &filename);

    int getNumOfIons() const;
    std::string getIonType(int num) const;

    void setGridSize(double grid_size);
    double getMinGridSize() const;

    void calculateBVS(int ion_type);

    std::string saveBVS(int ion_type, const std::string &output_dir);

private:
    void init();

private:
    std::unique_ptr<IonData> ion;
    std::unique_ptr<BVSData> bvs;
    std::unique_ptr<MapData> map;
    std::unique_ptr<MapPosData> pos;
};

}
