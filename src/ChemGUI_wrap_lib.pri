include(ChemGUI.pri)

INCLUDEPATH += "$${WRAP_INCLUDEDIR}"

LIBS += -L\"$${LIBDIR}\" -l$${WRAP_LIB}

win32-g++|unix {
    PRE_TARGETDEPS += $${LIBDIR}/lib$${WRAP_LIB}.a
}
else:win32 {
    PRE_TARGETDEPS += $${LIBDIR}/$${WRAP_LIB}.lib
}
