#include "chemgui_wrap.h"

#include <string>
#include <iostream>

void printUsage(char **argv) {
    std::cerr << "Usage: " << argv[0] << " input_file <command>" << std::endl;
    std::cerr << "Command:" << std::endl;
    std::cerr << "\tinfo" << std::endl;
    std::cerr << "\tcalc: ion_num grid_size output_dir" << std::endl;
}

void printInfo(const std::string &input_file) {
    chemgui::ChemGUIWrap wrapper(input_file);
    for (int i = 0; i < wrapper.getNumOfIons(); i++) {
        std::cout << wrapper.getIonType(i) << "\n";
    }
    std::cout << "\n" << wrapper.getMinGridSize() << std::endl;
}

void calc(const std::string &input_file, int ion_num, double grid_size, const std::string &output_dir) {
    chemgui::ChemGUIWrap wrapper(input_file);
    wrapper.setGridSize(grid_size);
    wrapper.calculateBVS(ion_num);
    const std::string output_file = wrapper.saveBVS(ion_num, output_dir);
    std::cout << output_file << std::endl;
}

int main(int argc, char **argv) {
    if (argc <= 2) {
        printUsage(argv);
        return -1;
    }

    const std::string input_file = argv[1];
    const std::string command = argv[2];

    if (command == "info") {
        printInfo(input_file);
    } else if (command == "calc") {
        const int ion_num = std::stoi(argv[3]);
        const double grid_size = std::stod(argv[4]);
        const std::string output_dir = argv[5];
        calc(input_file, ion_num, grid_size, output_dir);
    }

    return 0;
}
