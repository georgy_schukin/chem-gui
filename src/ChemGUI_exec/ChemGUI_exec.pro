TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$BINDIR
TARGET = bvscalc

include(../ChemGUI_wrap_lib.pri)
include(../ChemGUI_lib.pri)

SOURCES += main.cpp
