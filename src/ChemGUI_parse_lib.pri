include(ChemGUI.pri)

INCLUDEPATH += "$${PARSE_INCLUDEDIR}"

LIBS += -L\"$${LIBDIR}\" -l$${PARSE_LIB}

win32-g++|unix {
    PRE_TARGETDEPS += $${LIBDIR}/lib$${PARSE_LIB}.a
}
else:win32 {
    PRE_TARGETDEPS += $${LIBDIR}/$${PARSE_LIB}.lib
}
