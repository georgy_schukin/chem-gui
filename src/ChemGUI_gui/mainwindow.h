#pragma once

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

namespace model {
    class Model;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(model::Model *model, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionOpen_triggered();
    void on_actionExit_triggered();
    void on_actionAbout_triggered();

    void on_calc_bvs_button_clicked();

    void on_model_loaded(model::Model *model);

    void on_bv_grid_size_valueChanged(double arg1);

private:
    model::Model *model;
    Ui::MainWindow *ui;    
};

