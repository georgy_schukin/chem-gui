#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "model/model.h"
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(model::Model *model, QWidget *parent) :
    QMainWindow(parent),
    model(model),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(model, &model::Model::modelLoaded, this, &MainWindow::on_model_loaded);    
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_actionOpen_triggered() {
    QString name_filter = "WXYZ (*.wxyz);;All files (*.*)";
    QString filename = QFileDialog::getOpenFileName(this, "Open file", "", name_filter);
    if (!filename.isNull()) {
        model->loadFrom(filename);
    }
}

void MainWindow::on_model_loaded(model::Model *model) {
    ui->ion_type_list->clear();
    ui->ion_type_list->addItems(model->getIonTypes());
    ui->calc_bvs_button->setEnabled(true);
    ui->bv_grid_size->setMinimum(model->getMinGridSize());
    ui->bv_grid_size->setValue(ui->bv_grid_size->value()); // to set grid size in model
}

void MainWindow::on_actionExit_triggered() {
    qApp->exit();
}

void MainWindow::on_actionAbout_triggered() {
    QString about_msg = QString("ChemGUI app v1.0\n") +
                        QString("GUI: Georgy Schukin\n") +
                        QString("Math: Vladislav Komarov\n");
    QMessageBox::information(this, "ChemGUI", about_msg);
}

void MainWindow::on_calc_bvs_button_clicked() {
    QString output_dir = QFileDialog::getExistingDirectory(this, "Choose directory to save results",
                                                           "", QFileDialog::ShowDirsOnly);
    if (!output_dir.isNull()) {
        int ion_type = ui->ion_type_list->currentIndex();
        model->calculateBVS(ion_type, output_dir);
        QMessageBox::information(this, "ChemGUI", "Done!");
    }
}

void MainWindow::on_bv_grid_size_valueChanged(double grid_size) {
    model->setGridSize(grid_size);
}
