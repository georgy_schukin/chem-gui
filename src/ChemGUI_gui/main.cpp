#include "mainwindow.h"
#include "model/model.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    model::Model model;
    MainWindow w(&model);
    w.show();

    return a.exec();
}
