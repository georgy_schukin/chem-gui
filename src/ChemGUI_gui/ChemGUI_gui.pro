QT += core gui
CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ChemGUI_gui
TEMPLATE = app

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$BINDIR

include(../ChemGUI_lib.pri)

SOURCES += main.cpp\
    mainwindow.cpp \
    model/model.cpp

HEADERS  += mainwindow.h \
    model/model.h

FORMS    += mainwindow.ui

RESOURCES +=
