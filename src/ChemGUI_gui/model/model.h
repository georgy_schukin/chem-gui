#pragma once

#include <QObject>
#include <QScopedPointer>
#include <QStringList>

struct IonData;
struct BVSData;
struct MapData;
struct MapPosData;

namespace model {

class Model : public QObject {
    Q_OBJECT
public:
    Model(QObject *parent = 0);
    ~Model();

    void loadFrom(QString filename);
    QStringList getIonTypes() const;
    void calculateBVS(int ion_type, QString output_dir);

    void setGridSize(double grid_size);
    double getMinGridSize();

signals:
    void modelLoaded(Model *model);

private:
    void calculateBVS(int ion_type);
    void saveBVS(int ion_type, QString output_dir);

private:
    QScopedPointer<IonData> ion;
    QScopedPointer<BVSData> bvs;
    QScopedPointer<MapData> map;
    QScopedPointer<MapPosData> pos;
};

}
