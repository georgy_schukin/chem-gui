#include "model/model.h"
#include "chemgui.h"
#include <QDir>

namespace model {

Model::Model(QObject *parent) :
    QObject(parent)
{
    ion.reset(new IonData());
    bvs.reset(new BVSData());
    map.reset(new MapData());
    pos.reset(new MapPosData());
}

Model::~Model() {
}

void Model::loadFrom(QString filename) {
    QByteArray filename_8bit = filename.toLocal8Bit();
    read_data_from_file(filename_8bit.data(), ion.data(), map.data(), pos.data(), 0);
    emit modelLoaded(this);
}

QStringList Model::getIonTypes() const {
    QStringList result;
    for (int i = 0; i < ion->total_ion_num.both; i++) {
        result.append(QString(ion->ions[i].ion_label));
    }
    return result;
}

void Model::calculateBVS(int ion_type, QString output_dir) {
    calculateBVS(ion_type);
    saveBVS(ion_type, output_dir);
}

void Model::calculateBVS(int ion_type) {
    clear_bvs(bvs.data());
    calc_bvs(ion_type, ion.data(), bvs.data(), map.data(), pos.data(), 0);  // Calculate BVS for i-th ion type
}

void Model::saveBVS(int ion_type, QString output_dir) {
    QString ion_label(ion->ions[ion_type].ion_label);
    QDir dir(output_dir);
    QByteArray bvs_filename = dir.filePath(QString("%1.cub").arg(ion_label)).toLocal8Bit();
    QByteArray gii_filename = dir.filePath(QString("GII_%1.cub").arg(ion_label)).toLocal8Bit();
    QByteArray da_filename = dir.filePath(QString("DA_%1.cub").arg(ion_label)).toLocal8Bit();

    save_bvs_cube(bvs_filename.data(), ion_type, ion.data(), bvs.data(), map.data(), pos.data());
    save_gii_cube(gii_filename.data(), ion_type, ion.data(), bvs.data(), map.data(), pos.data());
    save_da_cube(da_filename.data(), ion_type, ion.data(), bvs.data(), map.data(), pos.data());
}

void Model::setGridSize(double grid_size) {
    set_grid_size(map.data(), static_cast<float>(grid_size));
}

double Model::getMinGridSize() {
    return static_cast<double>(get_min_grid_size(map.data()));
}

}
