#include "chemgui_table_gen.h"
#include "parse/scatter_factor.h"
#include "parse/bvs_table.h"

#include <vector>

int add_to_result(int current_index, float value, float *result, int result_max_size) {
    if (current_index >= result_max_size) {
        return current_index;
    }
    result[current_index] = value;
    return current_index + 1;
}

float get_value(enum TableGenType gen_type, const parse::ScatterFactor &sf1, const parse::ScatterFactor &sf2) {
    switch (gen_type) {
    case GEN_R0_TABLE:
        return parse::BVSTable::getR0(sf1, sf2);
    case GEN_B_TABLE:
        return parse::BVSTable::getB(sf1, sf2);
    case GEN_RC_TABLE:
        return parse::BVSTable::getRc(sf1, sf2);
    }
    return 0.0;
}

void generate_ion_table(enum TableGenType gen_type, const char scatter_factors[][SFAC_STR_SZ], int scatter_factors_size, float *result, int result_max_size) {
    int current_index = 0;
    std::vector<parse::ScatterFactor> sfs;
    for (int i = 0; i < scatter_factors_size; i++) {
        sfs.push_back(parse::ScatterFactor(scatter_factors[i]));
    }
    for (auto it = sfs.begin(); it != sfs.end(); it++) {
        for (auto it2 = sfs.begin(); it2 != sfs.end(); it2++) {
            if (it == it2) {
                current_index = add_to_result(current_index, 0.0f, result, result_max_size);
            } else {
                current_index = add_to_result(current_index, get_value(gen_type, *it, *it2), result, result_max_size);
            }
        }
    }
}
