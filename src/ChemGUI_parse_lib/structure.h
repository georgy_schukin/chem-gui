#ifndef CHEMGUI_STRUCTURE_H
#define CHEMGUI_STRUCTURE_H

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif

struct SymmetryMatrix {
    float data[3][4];  // [raw][column]
};

struct Matrix3x3 {
    float data[3][3];  // [raw][column]
};

struct Vector3 {
    float x, y, z;
};

enum AtomicDisplacementType {
    ADT_UNDEF = 0,
    ADT_ISO,
    ADT_ANISO
};

struct AtomicDisplacement {
    AtomicDisplacementType type; // type of atomic displacement parameter U (0 - undef, 1 - iso, 2 - aniso)
    float Ui; // isotropic or iso. equivelent U
    float Ua[6]; // anisotropic U11 U22 U33 U32 U31 U12
};

struct Atom {
    char type[80]; // atom type symbol
    Vector3 parent_coord;   // parent (independent) atom coordinate
    int number;    // element number in periodic table (0 = fake atom (e.g. position, transition point...),-1 = deleted atom)
    float mass;    // element mass (a.m.u.)
    int oxidation_state;    // oxidation state
    bool is_mobile;    // mobility (0 - stationar, 1 - mobile)
    bool is_disorded;    // disordering (0 - ordered, 1 - disordered)
    float site_occupation_factor;   // SHELX site occupation factor (for disordered models)
    int position_multiplicity;      // crystallographic multiplicity of the position
    float position_occupancy;       // occupancy of the position (sof*pos_multiplicity)
    float unified_atom_type_code;    // unified atom type code (10+ox.st.)*10000 + element number*10 + occupancy
    int bvs_type;     // index of BVS type
    int pos_index;    // position index (using with (PosType)PosArr)
    AtomicDisplacement atomic_displacement;  // atomic displacement parameters
};

struct PositionType {
    int multiplicity;      // multiplicity
    int wyckoff_symbol;      // Wyckoff symbol
    char point_symmetry[10]; // point symmetry
    SymmetryMatrix s; // general form of point (e.g. (-x+0.5, +y+0.5, z))
};

struct GeneratedAtom {
    Vector3 coord; // cartesian coordinates
    SymmetryMatrix sym_gen; // symmetry generator
    int parent_atom_index; // index of parent atom
};

enum BravaisLatticeType {
    BLT_P = 1,
    BLT_I,
    BLT_R,
    BLT_F,
    BLT_A,
    BLT_B,
    BLT_C
};

enum CentrosymmetryType {
    CST_NONCENTROSYMMETRIC = 0,
    CST_CENTROSYMMETRIC = 1,
    CST_NONSTANDARD_POSITION // -> CST_NONSTANDARD_DESCRIPTION
};

struct UnitCell {
    float a, b, c;
    float alpha, beta, gamma;
};

struct BVSType {
    int element_number;     // element number in periodic table (0 = fake atom (e.g. position, transition point...),-1 = deleted atom)
    int oxidation_state;    // oxidation state
    float site_occupation_factor;   // site occupation factor (for disordered models)
    float unified_atom_type_code;   // unified atom type code (10+ox.st.)*10000 + element number*10 + occupancy
    int positions_count;    // number of independent positions with this BVS type
    int first_example;      // index of first representative of this BVS type
};


#define SFAC_STR_SZ 6

struct StructureData {
    char title[256]; // structure title
    UnitCell unit_cell_parameters;   // unit cell parameters a, b, c [A], al, be, ga [deg]
    UnitCell unit_cell_std_deviations;  // unit cell standard deviations a, b, c [A], al, be, ga [deg]
    Matrix3x3 unit_cell_matrix; // columns for unit cell vectors (a,b,c); a along positive direction of x,
                                //   b in xy plane, y-positive halfplane, c in z-positive halfspace
    int formula_units_count; // number of formula units
    int space_group_number;  // space group number
    char space_group_hm_notation[20];  // sg H.-M. symbol
    int crystal_symmetry_system;   // crystall symmetry system (1-a,2-m,3-o,4-r,5-t,6-h,7-c)
    CentrosymmetryType centrosymmetry;  // 0 - noncentrosymmetric, 1 - centrosymmetric (cs in origin), 2 - cs with none-standart cs position
    BravaisLatticeType bravais_lattice_type;  // Bravais lattice type index (1-P,2-I,3-R,4-F,5-A,6-B,7-C)
    int bravais_translations_count; // number of additional Bravais translations
    Vector3 bravais_translations[4]; // additional Bravais translations
    int sym_ops_count; // number of translationally independent symm. operations
    struct SymmetryMatrix sym_ops[48]; // translationally independent symm. operations
    struct PositionType position_types[64]; // all positions of the space group, should be read from special tables
    struct Atom independent_atoms[1024];  // independent atoms
    int independent_atoms_count;  // independent atoms number
    int total_atoms_count;  // total atom number (including parent atoms)
    int deleted_atoms_count;  // number of deleted atoms in gA
    struct GeneratedAtom generated_atoms[1024*1024];  // generated atoms
    char scatter_factors[16][SFAC_STR_SZ];
    int scatter_factors_count;
    struct BVSType bvs_types[16]; // descryption of BVS types for the structure
    int bvs_types_count;          // number of defined BVS types
};

#ifdef __cplusplus
}
#endif

#endif
