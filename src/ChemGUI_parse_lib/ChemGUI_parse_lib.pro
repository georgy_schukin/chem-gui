#-------------------------------------------------
#
# Project created by QtCreator 2016-04-13T16:41:06
#
#-------------------------------------------------

QT       -= core gui

TEMPLATE = lib
CONFIG += staticlib c++11

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$LIBDIR
TARGET = $$PARSE_LIB

SOURCES += chemgui_parse.cpp \
    parse/structure_updater.cpp \
    parse/structure_util.cpp \
    parse/util.cpp \
    parse/sym_op_decoder.cpp \
    parse/shelx_parser.cpp \
    parse/scatter_factor.cpp \
    parse/periodic_table.cpp \
    parse/bvs_table.cpp \
    chemgui_table_gen.cpp \
    cryst_math.cpp \
    matr_math.cpp

HEADERS += chemgui_parse.h \
    structure.h \
    parse/structure_updater.h \
    parse/structure_util.h \
    parse/sym_op_decoder.h \
    parse/util.h \
    parse/shelx_parser.h \
    parse/scatter_factor.h \
    parse/periodic_table.h \
    parse/bvs_table.h \
    chemgui_table_gen.h \
    cryst_math.h \
    matr_math.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
