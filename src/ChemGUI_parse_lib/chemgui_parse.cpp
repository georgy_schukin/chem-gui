#include "chemgui_parse.h"
#include "parse/shelx_parser.h"

#include <fstream>
#include <stdexcept>
#include <string>

void parse_shelx(const char *filename, StructureData *data) {
    std::ifstream f_in(filename);
    if (!f_in.is_open()) {
        throw std::runtime_error("Cannot open file " + std::string(filename));
    }
    parse::ShelXParser parser;
    parser.parse(f_in, data);    
}
