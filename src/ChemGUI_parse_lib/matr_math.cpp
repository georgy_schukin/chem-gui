#include "structure.h"

void set_unit_matr3x4(SymmetryMatrix *A)
{
    A->data[0][0] = 1.0; A->data[0][1] = 0.0; A->data[0][2] = 0.0; A->data[0][3] = 0.0;
    A->data[1][0] = 0.0; A->data[1][1] = 1.0; A->data[1][2] = 0.0; A->data[1][3] = 0.0;
    A->data[2][0] = 0.0; A->data[2][1] = 0.0; A->data[2][2] = 1.0; A->data[2][3] = 0.0;
}

void set_inverce_matr3x4(SymmetryMatrix *A)
{
    A->data[0][0] =-1.0; A->data[0][1] = 0.0; A->data[0][2] = 0.0; A->data[0][3] = 0.0;
    A->data[1][0] = 0.0; A->data[1][1] =-1.0; A->data[1][2] = 0.0; A->data[1][3] = 0.0;
    A->data[2][0] = 0.0; A->data[2][1] = 0.0; A->data[2][2] =-1.0; A->data[2][3] = 0.0;
}

void set_translation_matr3x4(Vector3 *t,SymmetryMatrix *A)
{
    A->data[0][0] = 1.0; A->data[0][1] = 0.0; A->data[0][2] = 0.0;
    A->data[0][3] = t->x;
    A->data[1][0] = 0.0; A->data[1][1] = 1.0; A->data[1][2] = 0.0;
    A->data[1][3] = t->y;
    A->data[2][0] = 0.0; A->data[2][1] = 0.0; A->data[2][2] = 1.0;
    A->data[2][3] = t->z;
}

void copy_invert000_matr3x4(SymmetryMatrix *A, SymmetryMatrix *B)
{  // A' = I * A ( != A * I)
    B->data[0][0] = -A->data[0][0]; B->data[0][1] = -A->data[0][1];
    B->data[0][2] = -A->data[0][2]; B->data[0][3] = -A->data[0][3];
    B->data[1][0] = -A->data[1][0]; B->data[1][1] = -A->data[1][1];
    B->data[1][2] = -A->data[1][2]; B->data[1][3] = -A->data[1][3];
    B->data[2][0] = -A->data[2][0]; B->data[2][1] = -A->data[2][1];
    B->data[2][2] = -A->data[2][2]; B->data[2][3] = -A->data[2][3];
}

void set_vect3(float x, float y, float z, Vector3 *t)
{
    t->x = x; t->y = y; t->z = z;
}

void calc_matr3x3_vect3_prod(Matrix3x3 *A, Vector3 *b, Vector3 *e)
{ // e = A * b
    e->x = A->data[0][0] * b->x + A->data[0][1] * b->y + A->data[0][2] * b->z;
    e->y = A->data[1][0] * b->x + A->data[1][1] * b->y + A->data[1][2] * b->z;
    e->z = A->data[2][0] * b->x + A->data[2][1] * b->y + A->data[2][2] * b->z;
}

void calc_difference_vect3(Vector3 *b1, Vector3 *b2, Vector3 *e)
{
    e->x = b1->x - b2->x;
    e->y = b1->y - b2->y;
    e->z = b1->z - b2->z;
}

void calc_matr3x4_vect3_prod(SymmetryMatrix *A, Vector3 *b, Vector3 *e)
{ // e = A(Q) * b + A(T)
    e->x = A->data[0][0] * b->x + A->data[0][1] * b->y + A->data[0][2] * b->z + A->data[0][3];
    e->y = A->data[1][0] * b->x + A->data[1][1] * b->y + A->data[1][2] * b->z + A->data[1][3];
    e->z = A->data[2][0] * b->x + A->data[2][1] * b->y + A->data[2][2] * b->z + A->data[2][3];
}

void copy_vect3(Vector3 *b, Vector3 *e)
{
    e->x = b->x;
    e->y = b->y;
    e->z = b->z;
}

void copy_matr3x4(SymmetryMatrix *A, SymmetryMatrix *B)
{   // A -> B
    B->data[0][0] = A->data[0][0]; B->data[0][1] = A->data[0][1]; B->data[0][2] = A->data[0][2]; B->data[0][3] = A->data[0][3];
    B->data[1][0] = A->data[1][0]; B->data[1][1] = A->data[1][1]; B->data[1][2] = A->data[1][2]; B->data[1][3] = A->data[1][3];
    B->data[2][0] = A->data[2][0]; B->data[2][1] = A->data[2][1]; B->data[2][2] = A->data[2][2]; B->data[2][3] = A->data[2][3];
}

void add_vect3(Vector3 *b1, Vector3 *b2, Vector3 *e)
{
    e->x = b1->x + b2->x;
    e->y = b1->y + b2->y;
    e->z = b1->z + b2->z;
}
