#ifndef CHEMGUI_PARSE_H
#define CHEMGUI_PARSE_H

#include "structure.h"

#ifdef __cplusplus
extern "C" {
#endif

void parse_shelx(const char *filename, struct StructureData *data);

#ifdef __cplusplus
}
#endif

#endif
