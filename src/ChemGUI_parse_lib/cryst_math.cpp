#include "structure.h"
#include "matr_math.h"
#include <cmath>
#include <iostream>

float calc_unified_position_code(int ox_st, int el_num, float site_occ)
{
    return 10000 * (10 + ox_st) + 10 * el_num + site_occ;
}

void init_unit_cell_matrix(StructureData *data)
{ // some checks of a, b, c, alpha, beta, gamma should be added!
    const float deg_to_rad_coefficient = 3.14159265359 / 180.0; // rad = deg * coef
    float ca,cb,cg,sg,cbpg,cbmg;
    float a,b,c;
    float alpha,beta,gamma;

    alpha = data->unit_cell_parameters.alpha * deg_to_rad_coefficient;
    beta = data->unit_cell_parameters.beta * deg_to_rad_coefficient;
    gamma = data->unit_cell_parameters.gamma * deg_to_rad_coefficient;

    ca = cos(alpha);
    cb = cos(beta);
    cg = cos(gamma);
    sg = sin(gamma);
    cbpg = cos(beta + gamma);
    cbmg = cos(beta - gamma);

    a = data->unit_cell_parameters.a;
    b = data->unit_cell_parameters.b;
    c = data->unit_cell_parameters.c;

    data->unit_cell_matrix.data[0][0] = a;
    data->unit_cell_matrix.data[1][0] = 0.0;
    data->unit_cell_matrix.data[2][0] = 0.0;

    data->unit_cell_matrix.data[0][1] = b * cg;
    data->unit_cell_matrix.data[1][1] = b * sg;
    data->unit_cell_matrix.data[2][1] = 0.0;

    data->unit_cell_matrix.data[0][2] = c * cb;
    data->unit_cell_matrix.data[1][2] = c * (ca - cg*cb) / sg;
    data->unit_cell_matrix.data[2][2] = c * sqrt(2*ca*cb*cg - ca*ca - cbpg*cbmg) / sg;
}

void init_bravais_translation_vectors(StructureData *data)
{
    set_vect3(0.0,0.0,0.0,&data->bravais_translations[0]);
    switch (data->bravais_lattice_type) {
    case BLT_P:
        data->bravais_translations_count = 1;
        break;
    case BLT_I:
        set_vect3(0.5,0.5,0.5,&data->bravais_translations[1]);
        data->bravais_translations_count = 2;
        break;
    case BLT_R:
        set_vect3(0.3333333,0.6666667,0.3333333,&data->bravais_translations[1]);
        set_vect3(0.6666667,0.3333333,0.6666667,&data->bravais_translations[2]);
        data->bravais_translations_count = 3;
        break;
    case BLT_F:
        set_vect3(0.0,0.5,0.5,&data->bravais_translations[1]);
        set_vect3(0.5,0.0,0.5,&data->bravais_translations[2]);
        set_vect3(0.5,0.5,0.0,&data->bravais_translations[3]);
        data->bravais_translations_count = 4;
        break;
    case BLT_A:
        set_vect3(0.0,0.5,0.5,&data->bravais_translations[1]);
        data->bravais_translations_count = 2;
        break;
    case BLT_B:
        set_vect3(0.5,0.0,0.5,&data->bravais_translations[1]);
        data->bravais_translations_count = 2;
        break;
    case BLT_C:
        set_vect3(0.5,0.5,0.0,&data->bravais_translations[1]);
        data->bravais_translations_count = 2;
        break;
    default:
        // *** Wrong BL type! Generate an alert!
        return;
    }
}

void extend_shelx_symm_operator_list(StructureData *data)
{ // Add E-operator and inverted set of operatorators for centrosimmetric groups (positive LATT)
  //   designed of SHELX file processing

  // Add E (trivial) operation
  // *** add here check of existance of E-operation in sym_op
  set_unit_matr3x4(&data->sym_ops[data->sym_ops_count++]);

  // *** Check I operation betwean existed ones for NON-STANDARD space group defenition

  // Extend list of symmetry operators for centrosymmetric groups
  if (data->centrosymmetry==CST_CENTROSYMMETRIC)
  {
      // *** how to declare j inside for?
      int j = data->sym_ops_count;
      for (int i = 0; i < data->sym_ops_count; i++)
      {
          copy_invert000_matr3x4(&data->sym_ops[i], &data->sym_ops[j]);
          j++;
      }
      data->sym_ops_count *= 2;
  }
}

void init_bvs_types(StructureData *data)
{
    data->bvs_types_count = 0;
    for(int i = 0; i < data->independent_atoms_count; i++)
    {
        // *** should be initialized at reading!  check it!
        data->independent_atoms[i].unified_atom_type_code = calc_unified_position_code(
                    data->independent_atoms[i].oxidation_state,
                    data->independent_atoms[i].number,
                    data->independent_atoms[i].position_occupancy);

        bool new_bvs_type = true;
        for (int j = 0; j < data->bvs_types_count; j++)
        {
            if (data->bvs_types[j].unified_atom_type_code == data->independent_atoms[i].unified_atom_type_code)
            {
                // existing BVS type was found
                new_bvs_type = false;
                data->independent_atoms[i].bvs_type = j;
                data->bvs_types[j].positions_count++;
                break;
            }
        }
        if (new_bvs_type) {
            // new BVS type was found
            data->bvs_types[data->bvs_types_count].element_number =
                    data->independent_atoms[i].number;
            data->bvs_types[data->bvs_types_count].first_example = i;
            data->bvs_types[data->bvs_types_count].oxidation_state =
                    data->independent_atoms[i].oxidation_state;
            data->bvs_types[data->bvs_types_count].positions_count = 1;
            data->bvs_types[data->bvs_types_count].site_occupation_factor =
                    data->independent_atoms[i].position_occupancy;
            data->bvs_types[data->bvs_types_count].unified_atom_type_code =
                    data->independent_atoms[i].unified_atom_type_code;

            data->independent_atoms[i].bvs_type = data->bvs_types_count++;
        }
    }
}

void transl_reduce_vect3(Vector3 *b, Vector3 *e)
{
    e->x = b->x; e->y = b->y; e->z = b->z;
    while (e->x < 0.0) e->x += 1.0; while (e->x >= 1.0) e->x -= 1.0;
    while (e->y < 0.0) e->y += 1.0; while (e->y >= 1.0) e->y -= 1.0;
    while (e->z < 0.0) e->z += 1.0; while (e->z >= 1.0) e->z -= 1.0;
}

bool is_zero_fract_vect3(Vector3 *f, Matrix3x3 *uc)
{
    const float zerro_cutoff_sq = 1e-4; // *** square of zerro-length cutoff, Angstroms^2
    Vector3 b, c;

    if (f->x > 0.5) b.x = f->x - 1.0; else b.x = f->x; // to avoid processint of (0.999, 0.999, 0.999) as non-zero
    if (f->y > 0.5) b.y = f->y - 1.0; else b.y = f->y;
    if (f->z > 0.5) b.z = f->z - 1.0; else b.z = f->z;

    //std::cout << " dv (" << b.x << ", " << b.y << ", " << b.z << ")\n";

    calc_matr3x3_vect3_prod(uc, &b, &c);
    //std::cout << "..(" << c.x << ", " << c.y << ", " << c.z << ")" << c.x*c.x + c.y*c.y + c.z*c.z;

    if (c.x*c.x + c.y*c.y + c.z*c.z < zerro_cutoff_sq)
    {
        //std::cout << "..0\n";
        return true; // vector is almost zerro
    }
    //std::cout << "..X\n";
    return false; // vector is signisiacntly non-zerro
}

bool are_bravais_transl_equiv_vect3(Vector3 *r1, Vector3 *r2, Vector3 *rb, Matrix3x3 *uc)
{
    Vector3 a, b;
    calc_difference_vect3(r1, r2, &a);
    calc_difference_vect3(&a, rb, &b);
    transl_reduce_vect3(&b, &a);

    return is_zero_fract_vect3(&a, uc);
}

int calc_pos_multiplicity(StructureData *data, int pos_index)
{
    Vector3 r[256], rt; // coordinates of generated symmetrically-equivalent positions
    int rn = 0; // number of generated positions

    for (int i = 0; i < data->sym_ops_count; i++)
    {   // i - index of symmetry operation
        calc_matr3x4_vect3_prod(&data->sym_ops[i], &data->independent_atoms[pos_index].parent_coord, &rt);
        bool new_sep = true;  // flag of obtaining of new translationally independent symmetrically-equivalent position
        for (int j = 0; j < rn; j++)
        {   // j - index of generated tansl.-indep. symm.-equiv. vector
            for (int k = 0; k < data->bravais_translations_count; k++)
            {   // k - index of Bravais translation
                if (are_bravais_transl_equiv_vect3(&rt, &r[j], &data->bravais_translations[k], &data->unit_cell_matrix))
                {   // already generated tansl.-indep. symm.-equiv. vector was found
                    new_sep = false;
                    break;
                }
            }
            if (!new_sep) break;
        }
        if (new_sep)
        {   // new generated tansl.-indep. symm.-equiv. vector was found
            copy_vect3(&rt, &r[rn++]);
        }
    }
    return data->sym_ops_count / rn;
}

void init_shelx_multiplicity_occupancy(StructureData *data)
{
    int mult;

    for (int i = 0; i < data->independent_atoms_count; i++)
    {
        mult = calc_pos_multiplicity(data, i);
        std::cout << "Pos #" << i << "; multiplicity = " << mult << "; site occupancy = "
                  << data->independent_atoms[i].site_occupation_factor * mult << std::endl;
        data->independent_atoms[i].position_multiplicity = mult;
        data->independent_atoms[i].position_occupancy = data->independent_atoms[i].site_occupation_factor * mult;
    }
}

void fill_unit_cell(StructureData *data)
{
    int first_of_this_type_index, next_empty_index;
    Vector3 new_atom, rt1, rt2;

    data->total_atoms_count = data->deleted_atoms_count = 0;

    first_of_this_type_index = 0;
    next_empty_index = 0;
    for (int i = 0; i < 4 /*data->independent_atoms_count*/; i++)
    {  // i - index of independent atom
        for (int j = 0; j < data->sym_ops_count; j++)
        {   // j - index of symmetry operation
            for (int v = 0; v < data->bravais_translations_count; v++)
            {   // v - Bravais translation vector index
                calc_matr3x4_vect3_prod(&data->sym_ops[j], &data->independent_atoms[i].parent_coord, &rt2);
                add_vect3(&rt2, &data->bravais_translations[v], &rt1);
                transl_reduce_vect3(&rt1, &new_atom);
                bool is_new_atom = true;
                for (int k = first_of_this_type_index; k < next_empty_index; k++)
                {   // check if this position is already occupied
                    calc_difference_vect3(&new_atom, &data->generated_atoms[k].coord, &rt1);
                    transl_reduce_vect3(&rt1, &rt2);
                    if (is_zero_fract_vect3(&rt2, &data->unit_cell_matrix))
                    {
                        is_new_atom = false;
                        break;
                    }
                }
                if (is_new_atom)
                {   // new atom is generated, add it!
                    copy_vect3(&new_atom, &data->generated_atoms[next_empty_index].coord);
                    data->generated_atoms[next_empty_index].parent_atom_index = i;
                    copy_matr3x4(&data->sym_ops[j], &data->generated_atoms[next_empty_index].sym_gen);
                    data->total_atoms_count++;
                    next_empty_index++;
                    std::cout << "  atom " << i << " with coordinates (" << new_atom.x << ", " << new_atom.y << ", " << new_atom.z << ") was addad with index " << next_empty_index - 1 << " !!!\n";
                }
            }
        }
        std::cout << next_empty_index - first_of_this_type_index << " atoms of " << i << " were addad. \n";
        first_of_this_type_index = next_empty_index;
    }
}
