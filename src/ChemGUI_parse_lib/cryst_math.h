#ifndef CRYST_MATH_H
#define CRYST_MATH_H

void init_unit_cell_matrix(struct StructureData *data);
void extend_shelx_symm_operator_list(struct StructureData *data);
void init_bravais_translation_vectors(struct StructureData *data);
void init_bvs_types(struct StructureData *data);
void init_shelx_multiplicity_occupancy(struct StructureData *data);
void fill_unit_cell(struct StructureData *data);

#endif // CRYST_MATH_H
