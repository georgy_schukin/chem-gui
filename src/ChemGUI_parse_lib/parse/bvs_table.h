#pragma once

#include "scatter_factor.h"

namespace parse {

class BVSTable {
public:
    static double getR0(const ScatterFactor &s1, const ScatterFactor &s2);
    static double getB(const ScatterFactor &s1, const ScatterFactor &s2);
    static double getRc(const ScatterFactor &s1, const ScatterFactor &s2);
};

}
