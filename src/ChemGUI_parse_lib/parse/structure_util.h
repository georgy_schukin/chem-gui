#pragma once

#include <istream>
#include <ostream>

class StructureData;

std::ostream& operator<<(std::ostream &out, const StructureData &st);

