#pragma once

#include <fstream>
#include <memory>

class StructureData;

namespace parse {

class ShelXParser {
public:
    class Instruction {
    public:
        Instruction(const std::string &tag, const std::string &contents) :
            tag(tag), contents(contents) {
        }
        ~Instruction() {}

        const std::string& getTag() const {
            return tag;
        }

        const std::string& getContents() const {
            return contents;
        }

    private:
        std::string tag;
        std::string contents;
    };

public:
    ShelXParser();
    ~ShelXParser();

    void parse(std::istream &in, StructureData *data);

protected:
    std::shared_ptr<Instruction> nextInstruction(std::istream &in);
};

}

