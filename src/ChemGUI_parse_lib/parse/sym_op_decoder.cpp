#include "sym_op_decoder.h"

#include <iostream>
#include <regex>
#include <vector>

namespace parse {

namespace {
    float matchOP(const std::string &sym_op, char op) {
        std::regex op_regex(std::string("[\\+-]?") + op, std::regex_constants::icase);
        std::smatch match;
        if (std::regex_search(sym_op, match, op_regex)) {
            bool is_negative = (match.str().find('-') != std::string::npos);
            return (is_negative ? -1.0f : 1.0f);
        }
        return 0.0f;
    }

    float matchRealNumber(const std::string &sym_op) {
        std::regex fractional_number_regex("([\\+-]?\\d+)/(\\d+)");
        std::regex decimal_number_regex("[\\+-]?\\d+(\\.\\d+)?");
        std::smatch match;
        if (std::regex_search(sym_op, match, fractional_number_regex)) {
            float numerator = std::stof(match.str(1));
            float denominator = std::stof(match.str(2));
            return numerator/denominator;
        } else if (std::regex_search(sym_op, match, decimal_number_regex)) {
            return std::stof(match.str());
        }
        return 0.0f;
    }
}

std::vector<float> SymOpDecoder::decode(const std::string &sym_op) {
    std::vector<float> coeffs(4, 1.0f);
    std::vector<char> ops = {'x', 'y', 'z'};
    for (size_t i = 0; i < ops.size(); i++) {
        coeffs[i] = matchOP(sym_op, ops[i]);
    }
    coeffs[3] = matchRealNumber(sym_op);
    return coeffs;
}

}
