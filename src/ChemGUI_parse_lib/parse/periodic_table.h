#pragma once

#include <string>

namespace parse {

class PeriodicTable {
public:
    static double getElementMass(const std::string &elem);
    static int getElementNumber(const std::string &elem);
};

}
