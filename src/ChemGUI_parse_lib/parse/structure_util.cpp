#include "structure_util.h"
#include "structure.h"

#include <string>

namespace {
    std::string latticeTypeToString(const BravaisLatticeType &type) {
        switch (type) {
            case BLT_A: return "A";
            case BLT_B: return "B";
            case BLT_C: return "C";
            case BLT_F: return "F";
            case BLT_I: return "I";
            case BLT_P: return "P";
            case BLT_R: return "R";
            default: return "unknown";
        }
    }

    std::string centrosymmetryTypeToString(const CentrosymmetryType &type) {
        switch (type) {
            case CST_CENTROSYMMETRIC: return "centrosymmetric";
            case CST_NONCENTROSYMMETRIC: return "non-centrosymmetric";
            case CST_NONSTANDARD_POSITION: return "nonstandard position";
            default: return "unknown";
        }
    }

    std::string atomicDisplacementTypeToString(const AtomicDisplacementType &type) {
        switch (type) {
            case ADT_ANISO: return "anisotropic";
            case ADT_ISO: return "isotropic";
            case ADT_UNDEF: return "undefined";
            default: return "unknown";
        }
    }
}

std::ostream& operator<<(std::ostream &out, const UnitCell &cell) {
    out << cell.a << ", "
        << cell.b << ", "
        << cell.c << ", "
        << cell.alpha << ", "
        << cell.beta << ", "
        << cell.gamma;
    return out;
}

std::ostream& operator<<(std::ostream &out, const Vector3 &vec) {
    out << "(" << vec.x << ", " << vec.y << ", " << vec.z << ")";
    return out;
}

std::ostream& operator<<(std::ostream &out, const SymmetryMatrix &m) {
    out << "[";
    for (int i = 0; i < 3; i++) {
        out << "(";
        for (int j = 0; j < 3; j++) {
            out << m.data[i][j] << ", ";
        }
        out << m.data[i][3] << ") ";
    }
    out << "]";
    return out;
}

std::ostream& operator<<(std::ostream &out, const BravaisLatticeType &type) {
    out << latticeTypeToString(type) << "(" << static_cast<int>(type) << ")";
    return out;
}

std::ostream& operator<<(std::ostream &out, const CentrosymmetryType &type) {
    out << centrosymmetryTypeToString(type);
    return out;
}

std::ostream& operator<<(std::ostream &out, const AtomicDisplacement &displ) {
    if (displ.type == ADT_ISO) {
        out << displ.Ui;
    } else if (displ.type == ADT_ANISO) {
        out << "(";
        for (int i = 0; i < 5; i++) {
            out << displ.Ua[i] << ", ";
        }
        out << displ.Ua[5] << ")";
    }
    out << " " << atomicDisplacementTypeToString(displ.type);
    return out;
}

std::ostream& operator<<(std::ostream &out, const Atom &atom) {
    out << atom.type << " "
        << atom.number << " " << atom.mass << " " << atom.oxidation_state << " "
        << atom.parent_coord << " "
        << atom.site_occupation_factor << " "
        << atom.atomic_displacement;
    return out;
}

std::ostream& operator<<(std::ostream &out, const StructureData &st) {
    out << "Title: " << st.title
        << "\nUnit cell dimensions: " << st.unit_cell_parameters
        << "\nUnit cell std deviations: " << st.unit_cell_std_deviations
        << "\nFormula units count: " << st.formula_units_count
        << "\nBravais lattice type: " << st.bravais_lattice_type;
    out << "\nBravais translations count: " << st.bravais_translations_count
        << "\nBravais translations:";
    for (int i = 0; i < st.bravais_translations_count; i++) {
        out << "\n\t" << st.bravais_translations[i];
    }
    out << "\nCentrosymmetry: " << st.centrosymmetry;
    out << "\nSymmetry ops count: " << st.sym_ops_count
        << "\nSymmetry ops:";
    for (int i = 0; i < st.sym_ops_count; i++) {
        out << "\n\t" << st.sym_ops[i];
    }
    out << "\nIndependent atoms count: " << st.independent_atoms_count
        << "\nIndependent atoms:";
    for (int i = 0; i < st.independent_atoms_count; i++) {
        out << "\n\t" << st.independent_atoms[i];
    }
    out << "\nScatter factors count: " << st.scatter_factors_count
        << "\nScatter factors:";
    for (int i = 0; i < st.scatter_factors_count; i++) {
        out << "\n\t" << st.scatter_factors[i];
    }

    return out;
}

