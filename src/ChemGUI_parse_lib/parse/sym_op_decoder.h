#pragma once

#include <string>
#include <vector>

namespace parse {

class SymOpDecoder {
public:
    static std::vector<float> decode(const std::string &sym_op);
};

}

