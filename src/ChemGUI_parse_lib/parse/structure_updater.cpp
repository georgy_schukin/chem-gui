#include "structure_updater.h"
#include "structure.h"

namespace parse {

namespace {
    int getTranslationsCount(const BravaisLatticeType &type) {
        switch (type) {
            case BLT_P: return 0;
            case BLT_I: return 1;
            case BLT_R: return 2;
            case BLT_F: return 3;
            case BLT_A: return 1;
            case BLT_B: return 1;
            case BLT_C: return 1;
            default: return 0;
        }
    }

    void initTranslations(const BravaisLatticeType &type, Vector3 *translations) {
        switch (type) {
            case BLT_P:
                break;
            case BLT_I:
                translations[0] = {0.5f, 0.5f, 0.5f};
                break;
            case BLT_R:
                translations[0] = {1.0f/3.0f, 2.0f/3.0f, 1.0f/3.0f};
                translations[1] = {2.0f/3.0f, 1.0f/3.0f, 2.0f/3.0f};
                break;
            case BLT_F:
                translations[0] = {0.0f, 0.5f, 0.5f};
                translations[1] = {0.5f, 0.0f, 0.5f};
                translations[2] = {0.5f, 0.5f, 0.0f};
                break;
            case BLT_A:
                translations[0] = {0.0f, 0.5f, 0.5f};
                break;
            case BLT_B:
                translations[0] = {0.5f, 0.0f, 0.5f};
                break;
            case BLT_C:
                translations[0] = {0.5f, 0.5f, 0.0f};
                break;
        }
    }
}

void StructureUpdater::update(StructureData *st) {
    st->bravais_translations_count = getTranslationsCount(st->bravais_lattice_type);
    initTranslations(st->bravais_lattice_type, st->bravais_translations);
}

}
