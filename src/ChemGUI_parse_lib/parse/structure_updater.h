#pragma once

class StructureData;

namespace parse {

class StructureUpdater {
public:
    StructureUpdater() {}

    void update(StructureData *st);
};

}

