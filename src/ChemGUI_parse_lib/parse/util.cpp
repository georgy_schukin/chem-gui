#include "util.h"

#include <algorithm>
#include <cstdio>
#include <sstream>

namespace parse {
namespace util {

namespace {
    template <class Pred>
    std::string stripLeft(std::string &str, Pred pred) {
        str.erase(str.begin(), std::find_if(str.begin(), str.end(), pred));
        return str;
    }

    template <class Pred>
    std::string stripRight(std::string &str, Pred pred) {
        str.erase(std::find_if(str.rbegin(), str.rend(), pred).base(), str.end());
        return str;
    }

    bool isNotSpace(char &ch) {
        return !isspace(ch);
    }
}

std::string stripLeft(std::string &str) {
    return stripLeft(str, isNotSpace);
}

std::string stripLeft(std::string &str, char ch) {
    return stripLeft(str, [ch](char &c) { return c != ch; });
}

std::string stripRight(std::string &str) {
    return stripRight(str, isNotSpace);
}

std::string stripRight(std::string &str, char ch) {
    return stripRight(str, [ch](char &c) { return c != ch; });
}

std::string trim(std::string &str) {
    stripLeft(str);
    stripRight(str);
    return str;
}

std::string trim(std::string &str, char ch) {
    stripLeft(str, ch);
    stripRight(str, ch);
    return str;
}

std::string tolower(const std::string &str) {
    using namespace std;
    std::string result = str;
    std::transform(result.begin(), result.end(), result.begin(), ::tolower);
    return result;
}

std::string getLine(std::istream &in) {
    std::string result;
    std::getline(in, result);
    if (result.back() == '\r') {
        result.erase(std::prev(result.end()));
    }
    return result;
}

std::string getLineTrimmed(std::istream &in) {
    std::string line = getLine(in);
    return trim(line);
}

void getCLineTrimmed(std::istream &in, char *str, size_t max_size) {
    std::string line = getLineTrimmed(in);
    snprintf(str, max_size, "%s", line.c_str());
}

std::vector<std::string> split(const std::string &str, char delim) {
    std::vector<std::string> result;
    std::stringstream ss;
    ss.str(str);
    std::string item = "";
    while (std::getline(ss, item, delim)) {
        if (!item.empty()) {
            result.push_back(item);
        }
    }
    return result;
}

}
}
