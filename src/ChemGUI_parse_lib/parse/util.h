#pragma once

#include <string>
#include <istream>
#include <cstddef>
#include <vector>

namespace parse {
namespace util {

std::string stripLeft(std::string &str);
std::string stripLeft(std::string &str, char ch);
std::string stripRight(std::string &str);
std::string stripRight(std::string &str, char ch);
std::string trim(std::string &str);
std::string trim(std::string &str, char ch);
std::string tolower(const std::string &str);

std::string getLine(std::istream &in);
std::string getLineTrimmed(std::istream &in);
void getCLineTrimmed(std::istream &in, char *str, size_t max_size);

std::vector<std::string> split(const std::string &str, char delim = ' ');

}
}
