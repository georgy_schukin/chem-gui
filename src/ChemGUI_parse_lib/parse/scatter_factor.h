#pragma once

#include <string>

namespace parse {

class ScatterFactor {
public:
    ScatterFactor();
    ScatterFactor(const std::string &str);

    const std::string& getAtomType() const;

    int getOxidationState() const;
    int getNumberInPeriodicTable() const;
    double getMass() const;

    const std::string& toString() const;

private:
    void parseFrom(const std::string &str);

private:
    std::string str;
    std::string atom_type;
    int oxidation_state;
};

}
