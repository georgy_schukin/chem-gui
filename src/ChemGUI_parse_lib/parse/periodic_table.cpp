#include "periodic_table.h"
#include "util.h"

#include <map>
#include <iostream>
#include <sstream>

namespace parse {

namespace {
    struct ElemInfo {
        int number;
        char symbol[4];
        double mass;
    };

    typedef std::map<std::string, ElemInfo> ElemTable;

    ElemTable getTable() {
        const std::string table_str =
                R"table(
                1	T	1	1	3.0	1
                1	D	1	1	2.0	0
                1	H	1	1	1.00794	0
                2	He	1	18	4.002602	0
                3	Li	2	1	6.941	0
                4	Be	2	2	9.012182	0
                5	B	2	13	10.811	0
                6	C	2	14	12.0107	0
                7	N	2	15	14.0067	0
                8	O	2	16	15.9994	0
                9	F	2	17	18.9984032	0
                10	Ne	2	18	20.1797	0
                11	Na	3	1	22.98976928	0
                12	Mg	3	2	24.3050	0
                13	Al	3	13	26.9815386	0
                14	Si	3	14	28.0855	0
                15	P	3	15	30.973762	0
                16	S	3	16	32.065	0
                17	Cl	3	17	35.453	0
                18	Ar	3	18	39.948	0
                19	K	4	1	39.0983	0
                20	Ca	4	2	40.078	0
                21	Sc	4	3	44.955912	0
                22	Ti	4	4	47.867	0
                23	V	4	5	50.9415	0
                24	Cr	4	6	51.9961	0
                25	Mn	4	7	54.938045	0
                26	Fe	4	8	55.845	0
                27	Co	4	9	58.933195	0
                28	Ni	4	10	58.6934	0
                29	Cu	4	11	63.546	0
                30	Zn	4	12	65.409	0
                31	Ga	4	13	69.723	0
                32	Ge	4	14	72.64	0
                33	As	4	15	74.92160	0
                34	Se	4	16	78.96	0
                35	Br	4	17	79.904	0
                36	Kr	4	18	83.798	0
                37	Rb	5	1	85.4678	0
                38	Sr	5	2	87.62	0
                39	Y	5	3	88.90585	0
                40	Zr	5	4	91.224	0
                41	Nb	5	5	92.90638	0
                42	Mo	5	6	95.94	0
                43	Tc	5	7	98.9063	1
                44	Ru	5	8	101.07	0
                45	Rh	5	9	102.90550	0
                46	Pd	5	10	106.42	0
                47	Ag	5	11	107.8682	0
                48	Cd	5	12	112.411	0
                49	In	5	13	114.818	0
                50	Sn	5	14	118.710	0
                51	Sb	5	15	121.760	0
                52	Te	5	16	127.60	0
                53	I	5	17	126.90447	0
                54	Xe	5	18	131.293	0
                55	Cs	6	1	132.9054519	0
                56	Ba	6	2	137.327	0
                57	La	6	3	138.90547	0
                58	Ce	6	3	140.116	0
                59	Pr	6	3	140.90765	0
                60	Nd	6	3	144.242	0
                61	Pm	6	3	146.9151	1
                62	Sm	6	3	150.36	0
                63	Eu	6	3	151.964	0
                64	Gd	6	3	157.25	0
                65	Tb	6	3	158.92535	0
                66	Dy	6	3	162.500	0
                67	Ho	6	3	164.93032	0
                68	Er	6	3	167.259	0
                69	Tm	6	3	168.93421	0
                70	Yb	6	3	173.04	0
                71	Lu	6	33	174.967	0
                72	Hf	6	4	178.49	0
                73	Ta	6	5	180.9479	0
                74	W	6	6	183.84	0
                75	Re	6	7	186.207	0
                76	Os	6	8	190.23	0
                77	Ir	6	9	192.217	0
                78	Pt	6	10	195.084	0
                79	Au	6	11	196.966569	0
                80	Hg	6	12	200.59	0
                81	Tl	6	13	204.3833	0
                82	Pb	6	14	207.2	0
                83	Bi	6	15	208.98040	0
                84	Po	6	16	208.9824	1
                85	At	6	17	209.9871	1
                86	Rn	6	18	222.0176	1
                87	Fr	7	1	223.0197	1
                88	Ra	7	2	226.0254	1
                89	Ac	7	3	227.0278	1
                90	Th	7	3	232.03806	1
                91	Pa	7	3	231.03588	1
                92	U	7	3	238.02891	1
                93	Np	7	3	237.0482	1
                94	Pu	7	3	244.0642	1
                95	Am	7	3	243.0614	1
                96	Cm	7	3	247.0703	1
                97	Bk	7	3	247.0703	1
                98	Cf	7	3	251.0796	1
                99	Es	7	3	252.0829	1
                100	Fm	7	3	257.0951	1
                101	Md	7	3	258.0986	1
                102	No	7	3	259.1009	1
                103	Lr	7	3	262	1
                104	Rf	7	4	267	1
                105	Db	7	5	268	1
                106	Sg	7	6	271	1
                107	Bh	7	7	270	1
                108	Hs	7	8	269	1
                109	Mt	7	9	278	1
                110	Ds	7	10	281	1
                111	Rg	7	11	282	1
                112	Cn	7	12	283	1
                113	Nh	7	13	284	1
                114	Fl	7	14	289	1
                115	Mc	7	15	289	1
                116	Lv	7	16	292	1
                117	Ts	7	17	294	1
                118	Og	7	18	294	1
                )table";

        ElemTable table;
        int temp;
        for (auto str: util::split(table_str, '\n')) {
            std::istringstream in(str);
            ElemInfo elem;
            in >> elem.number >> elem.symbol >> temp >> temp >> elem.mass >> temp;
            table[util::tolower(elem.symbol)] = elem;
        }
        return table;
    }

    const ElemInfo& getElemInfo(const std::string &elem) {
        static const ElemInfo empty = {0, "", 0};
        static const auto table = getTable();
        auto it = table.find(util::tolower(elem));
        return (it != table.end() ? (*it).second : empty);
    }
}

double PeriodicTable::getElementMass(const std::string &elem) {
    return getElemInfo(elem).mass;
}

int PeriodicTable::getElementNumber(const std::string &elem) {
    return getElemInfo(elem).number;
}

}
