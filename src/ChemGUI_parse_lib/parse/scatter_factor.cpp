#include "scatter_factor.h"
#include "periodic_table.h"

#include <regex>

namespace parse {

ScatterFactor::ScatterFactor():
    str(""), atom_type(""), oxidation_state(0) {
}

ScatterFactor::ScatterFactor(const std::string &str) :
    str(str), atom_type(""), oxidation_state(0)
{
    parseFrom(str);
}

const std::string& ScatterFactor::getAtomType() const {
    return atom_type;
}

int ScatterFactor::getOxidationState() const {
    return oxidation_state;
}

int ScatterFactor::getNumberInPeriodicTable() const {
    return PeriodicTable::getElementNumber(getAtomType());
}

double ScatterFactor::getMass() const {
    return PeriodicTable::getElementMass(getAtomType());
}

const std::string& ScatterFactor::toString() const {
    return str;
}

void ScatterFactor::parseFrom(const std::string &str) {
    std::regex atom_type_regex("[a-zA-Z]+");
    std::regex oxid_state_regex_1("[\\+-](\\d+)");
    std::regex oxid_state_regex_2("(\\d+)[\\+-]");
    std::smatch match;
    if (std::regex_search(str, match, atom_type_regex)) {
        atom_type = match.str(0);
    }
    if (std::regex_search(str, match, oxid_state_regex_1) ||
        std::regex_search(str, match, oxid_state_regex_2)) {
        oxidation_state = std::stoi(match.str(1));
        if (match.str(0).find("-") != std::string::npos) {
            oxidation_state = -oxidation_state;
        }
    }
}

}
