#include "shelx_parser.h"
#include "structure.h"
#include "util.h"
#include "sym_op_decoder.h"
#include "structure_updater.h"
#include "scatter_factor.h"

#include <fstream>
#include <map>
#include <string>
#include <functional>
#include <cmath>
#include <vector>
#include <cassert>
#include <sstream>
#include <iostream>
#include <list>

namespace parse {

typedef std::function<void(std::istream&, StructureData*)> Handler;
typedef std::map<std::string, Handler> HandlerMap;

namespace {
    int current_sym_ops_count = 0;
    int current_atoms_count = 0;
    bool atoms_are_active = true;
    float overall_scale_factor = 0;
    std::vector<ScatterFactor> scatter_factors;
    std::vector<float> free_variables;

    const std::vector<std::string> instruction_names = {
        "REM" ,"CELL","ZERR","LATT","SYMM","SFAC","DISP",
        "UNIT","LAUE","MORE","TIME","HKLF","OMIT","SHEL","BASF","TWIN","EXTI",
        "SWAT","HOPE","MERG","SPEC","RESI","RTAB","MPLA","HFIX","MOVE","ANIS",
        "AFIX","FRAG","FEND","EXYZ","EADP","PART","EQIV","CONF","CONN","BIND",
        "FREE","DFIX","DANG","BUMP","SAME","SADI","CHIV","FLAT","DELU","SIMU",
        "DEFS","ISOR","NCSY","SUMP","L.S.","CGLS","BLOC","DAMP","STIR","WGHT",
        "FVAR","BOND","HTAB","LIST","ACTA","SIZE","TEMP","WPDB","FMAP","GRID",
        "PLAN","MOLE","END"};

    bool isAtomName(const std::string &name) {
        if (!atoms_are_active) {
            return false;
        }
        for (const auto &instruction_name: instruction_names) {
            if (instruction_name == name) {
                return false;
            }
        }
        return true;
    }

    void prepareForParsing(StructureData *data) {
        data->sym_ops_count = 0;
        data->independent_atoms_count = 0;
        data->scatter_factors_count = 0;
        atoms_are_active = true;
        overall_scale_factor = 0;
        scatter_factors.clear();
        free_variables.clear();
    }

    std::istream& operator>>(std::istream &in, UnitCell &cell) {
        in >> cell.a >> cell.b >> cell.c
            >> cell.alpha >> cell.beta >> cell.gamma;
        return in;
    }

    std::istream& operator>>(std::istream &in, Vector3 &v) {
        in >> v.x >> v.y >> v.z;
        return in;
    }

    std::string readSymOP(std::istream &in) {
        std::string sym_op;
        in >> sym_op;
        return util::trim(sym_op, ',');
    }

    std::istream& operator>>(std::istream &in, SymmetryMatrix &m) {
        for (int i = 0; i < 3; i++) {
            const std::vector<float> coeffs = SymOpDecoder::decode(readSymOP(in));
            assert (coeffs.size() == 4);
            for (int j = 0; j < 4; j++) {
                m.data[i][j] = coeffs[j];
            }
        }
        return in;
    }

    void handleTitle(std::istream &in, StructureData *data) {
        util::getCLineTrimmed(in, data->title, sizeof(data->title));
    }

    void handleCell(std::istream &in, StructureData *data) {
        float wavelength;
        in >> wavelength;
        in >> data->unit_cell_parameters;
    }

    void handleZerr(std::istream &in, StructureData *data) {
        in >> data->formula_units_count;
        in >> data->unit_cell_std_deviations;
    }

    void handleLatticeType(std::istream &in, StructureData *data) {
        int lattice_type;
        in >> lattice_type;
        data->bravais_lattice_type = static_cast<BravaisLatticeType>(abs(lattice_type));
        data->centrosymmetry = lattice_type > 0 ? CST_CENTROSYMMETRIC : CST_NONCENTROSYMMETRIC;
    }

    void handleSymmetry(std::istream &in, StructureData *data) {
        in >> data->sym_ops[data->sym_ops_count++];
    }    

    void handleScatterFactors(std::istream &in, StructureData *data) {
        std::string factor_str;
        while (in >> factor_str) {
            ScatterFactor sfac(factor_str);
            scatter_factors.push_back(sfac);
            int curr_sfac_num = data->scatter_factors_count++;
            sprintf(data->scatter_factors[curr_sfac_num], "%s", sfac.toString().c_str());
        }        
    }

    void handleHKLF(std::istream &in, StructureData *data) {
        atoms_are_active = false;
    }

    void handleFreeVariables(std::istream &in, StructureData *data) {
        in >> overall_scale_factor;
        float free_variable;
        while (in >> free_variable) {
            free_variables.push_back(free_variable);
        }
    }

    float applyFreeVariable(float value) {
        const float abs_value = fabs(value);
        if (abs_value > 15) {
            int free_var_num = 0;
            while (10*free_var_num <= abs_value) {
                free_var_num++;
            }
            free_var_num--; // var num is abs value
            assert (free_var_num > 1);
            const float p = abs_value - 10*free_var_num; // p is abs value
            assert (p >= 0.0 && p < 5.0);
            if (value > 0) {
                return p*free_variables[free_var_num - 2];
            } else {
                return p*(1.0 - free_variables[free_var_num - 2]);
            }
        } else if (abs_value > 10) {
            return value > 0 ? value - 10 : value + 10;
        }
        return value;
    }

    void handleAtom(std::istream &in, StructureData *data) {
        Atom *atom = &data->independent_atoms[data->independent_atoms_count++];

        int sfac_num;
        in >> sfac_num;

        const ScatterFactor &sfac = scatter_factors[sfac_num - 1];

        sprintf(atom->type, "%s", sfac.getAtomType().c_str());

        atom->number = sfac.getNumberInPeriodicTable();
        atom->mass = sfac.getMass();
        atom->oxidation_state = sfac.getOxidationState();

        in >> atom->parent_coord;

        std::list<float> rest;
        float num;
        while (in >> num) {
            rest.push_back(num);
        }
        if (!rest.empty()) {
            atom->site_occupation_factor = applyFreeVariable(rest.front());
            rest.pop_front();
        }
        if (rest.size() > 1) {
            atom->atomic_displacement.type = ADT_ANISO;
            for (int i = 0; i < 6; i++) {
                atom->atomic_displacement.Ua[i] = applyFreeVariable(rest.front());
                rest.pop_front();
            }
        } else if (rest.size() == 1) {
            atom->atomic_displacement.type = ADT_ISO;
            atom->atomic_displacement.Ui = applyFreeVariable(rest.front());
            rest.pop_front();
        } else {
            atom->atomic_displacement.type = ADT_UNDEF;
        }
    }

    HandlerMap makeHandlers() {
        HandlerMap result;
        result["TITL"] = &handleTitle;
        result["CELL"] = &handleCell;
        result["ZERR"] = &handleZerr;
        result["LATT"] = &handleLatticeType;
        result["SYMM"] = &handleSymmetry;
        result["SFAC"] = &handleScatterFactors;
        result["HKLF"] = &handleHKLF;
        result["FVAR"] = &handleFreeVariables;
        return result;
    }  

    void postUpdate(StructureData *data) {                
        StructureUpdater().update(data);
    }        
}

ShelXParser::ShelXParser() {
}

ShelXParser::~ShelXParser() {
}

void ShelXParser::parse(std::istream &in, StructureData *data) {
    static auto handlers = makeHandlers();    

    prepareForParsing(data);

    std::shared_ptr<Instruction> instruction = nullptr;
    while ((instruction = nextInstruction(in))) {
        const std::string &tag = instruction->getTag();

        //std::cout << instruction->getTag() << " " << instruction->getContents() << std::endl;

        std::istringstream str_in(instruction->getContents());
        if (handlers.find(tag) != handlers.end()) {
            handlers[tag](str_in, data);
        } else if (isAtomName(tag)) {
            handleAtom(str_in, data);
        }
    }

    postUpdate(data);
}

std::shared_ptr<ShelXParser::Instruction> ShelXParser::nextInstruction(std::istream &in) {
    std::string tag;
    if (!(in >> tag) || tag.empty()) {
        return nullptr; // EOF
    }

    std::string contents = util::getLineTrimmed(in);

    size_t eq_pos = std::string::npos;
    while ((eq_pos = contents.find_last_of("=")) != std::string::npos) {
        contents.erase(eq_pos);
        contents += util::getLineTrimmed(in); // read next line
    }

    return std::make_shared<ShelXParser::Instruction>(tag, contents);
}

}
