#ifndef CHEMGUI_TABLE_GEN_H
#define CHEMGUI_TABLE_GEN_H

#include "structure.h"

#ifdef __cplusplus
extern "C" {
#endif

enum TableGenType {
    GEN_R0_TABLE,
    GEN_B_TABLE,
    GEN_RC_TABLE
};

void generate_ion_table(enum TableGenType gen_type, const char scatter_factors[][SFAC_STR_SZ], int scatter_factors_size, float *result, int result_max_size);

#ifdef __cplusplus
}
#endif

#endif
