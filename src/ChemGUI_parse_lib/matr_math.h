#ifndef MATR_MATH_H
#define MATR_MATH_H

void set_vect3(float x, float y, float z, struct Vector3 *t);
void set_unit_matr3x4(struct SymmetryMatrix *A);
void copy_invert000_matr3x4(struct SymmetryMatrix *A, struct SymmetryMatrix *B);
void calc_matr3x3_vect3_prod(struct Matrix3x3 *A, struct Vector3 *b, struct Vector3 *e);
void calc_difference_vect3(struct Vector3 *b1, struct Vector3 *b2, struct Vector3 *e);
void calc_matr3x4_vect3_prod(struct SymmetryMatrix *A, struct Vector3 *b, struct Vector3 *e);
void copy_vect3(struct Vector3 *b, struct Vector3 *e);
void copy_matr3x4(struct SymmetryMatrix *A, struct SymmetryMatrix *B);
void add_vect3(struct Vector3 *b1, struct Vector3 *b2, struct Vector3 *e);


#endif // MATR_MATH_H
