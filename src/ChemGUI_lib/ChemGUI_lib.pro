TEMPLATE = lib
CONFIG += staticlib
CONFIG -= app_bundle
CONFIG -= qt

exists(../ChemGUI.pri) {
    include(../ChemGUI.pri)
}

DESTDIR = $$LIBDIR
TARGET = $$CHEM_LIB

SOURCES += \    
    chem/calc.c \
    chem/read.c \
    chem/save.c

HEADERS += \    
    chemgui.h \
    chem/calc.h \
    chem/defines.h \
    chem/read.h \
    chem/save.h
