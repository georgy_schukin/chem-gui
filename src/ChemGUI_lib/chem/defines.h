#ifndef CHEMGUI_DEFINES
#define CHEMGUI_DEFINES

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_POS_NUM 32768
#define MAX_MAP_DIM 256
#define MAX_ION_TYPE 32
#define DELTA_FNM 0.001 // delta (in Angstroms) for numerical differentiation
#define DELTA_FNM_X2 0.002

#define ION_ANION_SIGN (-1)
#define ION_CATION_SIGN 1
#define ION_BOTH_SIGN 0

struct Ion {
    int charge;   // ion charge;
    int charge_sign;   // ion charge sign -1 - anion, +1 - cation, 0 - both
    int position_number;   // number of position, 0 if none-defined
    float occupation_factor;   // occupation factor, 0.0 if none-defined
    char ion_label[5]; // ion label
};

struct PositionType {
    int t;
    float r[3]; // [0] - x, [1] - y, [2] - z
};

struct PositionRange {
    int start;
    int end;
};

struct IonNumber {
    int anions;
    int cations;
    int both;
};

struct MapData {
    float uc_coord[8][3]; // coordinates of u.c. vertices
                        // [0][] - (0,0,0); [1][] - (1,0,0); [2][] - (0,1,0); [3][] - (1,1,0);
                        // [4][] - (0,0,1); [5][] - (1,0,1); [6][] - (0,1,1); [7][] - (1,1,1);
                        // [][0] - x, [][1] - y, [][2] - z
    float box_dim[3]; // Map box dimentions [0] - x, [1] - y, [2] - z
    float coord_inc[3]; // Increments for coordinate vectors [0] - x, [1] - y, [2] - z
    int node_num[3];  // Number of BVS map nodes    
    float box_dim_limit[2][2][3]; // limits of [0][][] - map box and [1][][] atom box borders
                              // [][0][] - lower limits, [][1][] - upper limits
                              // [][][0] - x limits, [][][1] - y limits, [][][2] - z limits
};

struct MapPosData {
    struct PositionType positions[MAX_POS_NUM]; // List of positions in atom box
    struct PositionRange ion_type_indices_for_pos[MAX_POS_NUM + 1]; // Start and end index of ion types for specified position type
    int pos_type_num; // Number of position types
    long long pos_num; // Number of positions were read into positions array
};

struct IonData {
    struct IonNumber basic_ion_num, additional_ion_num, total_ion_num;
    struct Ion ions[MAX_ION_TYPE]; // Ions description
    float ion_r0[MAX_ION_TYPE][MAX_ION_TYPE];  // Ion-ion R0 table
    float ion_b[MAX_ION_TYPE][MAX_ION_TYPE];  // Ion-ion b table
    float ion_r_cutoff[MAX_ION_TYPE][MAX_ION_TYPE];  // Ion-ion R_cutoff table
};

struct BVSData {
    float bvs_map[MAX_MAP_DIM][MAX_MAP_DIM][MAX_MAP_DIM];
    float bvs_da[MAX_MAP_DIM][MAX_MAP_DIM][MAX_MAP_DIM];
    float bvs_ngh[MAX_MAP_DIM][MAX_MAP_DIM][MAX_MAP_DIM][6];
    float bvs_map_ext[2]; // [0] - Map min, [15] - Map max
};

#ifdef __cplusplus
}
#endif

#endif
