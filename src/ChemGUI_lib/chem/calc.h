#ifndef CHEMGUI_CALC
#define CHEMGUI_CALC

#include "defines.h"

#ifdef __cplusplus
extern "C" {
#endif

void calc_bvs(int ion_type, struct IonData *ion, struct BVSData *bvs,
              struct MapData *map, struct MapPosData *pos, int verbose);

void calc_bvs_cart(int ion_type, float *coord, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos);

void calc_bvs_pos(int ion_type, int pos_number, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos);

void calc_bvs_flst(int ion_type, char *filename, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos);

void clear_bvs(struct BVSData *bvs);
void clear_farray(float *arr, long int num);

float get_min_grid_size(const struct MapData *map);
void set_grid_size(struct MapData *map, float grid_size);

#ifdef __cplusplus
}
#endif

#endif

