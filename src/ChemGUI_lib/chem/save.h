#ifndef CHEMGUI_SAVE
#define CHEMGUI_SAVE

#include "defines.h"

#ifdef __cplusplus
extern "C" {
#endif

void get_cub_filename(const char *base, const char *tag, const char *ion_label, char *filename);

void save_da_cube(const char *filename, int ion_type,
                  struct IonData *ion, struct BVSData *bvs,
                  struct MapData *map, struct MapPosData *pos);

void save_bvs_cube(const char *filename, int ion_type,
                   struct IonData *ion, struct BVSData *bvs,
                   struct MapData *map, struct MapPosData *pos);

void save_gii_cube(const char *filename, int ion_type,
                   struct IonData *ion, struct BVSData *bvs,
                   struct MapData *map, struct MapPosData *pos);

#ifdef __cplusplus
}
#endif

#endif

