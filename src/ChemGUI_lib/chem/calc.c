#include "calc.h"
#include "defines.h"

#include <stdio.h>
#include <math.h>

void calc_bvs_map_ngh(int ion_type, struct IonData *ion, struct BVSData *bvs,
                  struct MapData *map, struct MapPosData *pos) {
    int ip, ii, ix, iy, iz, pt;
    int x1, x2, y1, y2, z1, z2;
    int kk;
    float rx, rxa, rx0, drx;
    float dy2, ry, rya, ry0, dry;
    float dz2, rz, rza, rz0, drz;
    float nd2[6]; // 0..6 -x, +x, -y, +y, -z, +z delta = DFNM
    float dz2p, dz2m, dy2p, dy2m; // 0..6 -x, +x, -y, +y, -z, +z delta = DFNM
    float d2;
    float rc, rc2;    

    rx0 = map->box_dim_limit[0][0][0];
    ry0 = map->box_dim_limit[0][0][1];
    rz0 = map->box_dim_limit[0][0][2];

    drx = map->coord_inc[0];
    dry = map->coord_inc[1];
    drz = map->coord_inc[2];

    x1 = 0; x2 = map->node_num[0];
    y1 = 0; y2 = map->node_num[1];
    z1 = 0; z2 = map->node_num[2];

    for (ip = 0; ip < pos->pos_num; ip++) {
        pt = pos->positions[ip].t - 1;

        rxa = pos->positions[ip].r[0];
        rya = pos->positions[ip].r[1];
        rza = pos->positions[ip].r[2];

        for (ii = pos->ion_type_indices_for_pos[pt].start; ii <= pos->ion_type_indices_for_pos[pt].end; ii++) {
            rc = ion->ion_r_cutoff[ion_type][ii];
            if (rc <= 0.0f) {
                continue;
            }
            rc2 = rc*rc;
            rz = rz0 + z1*drz; //rz=BoxDimLim[0][0][2]

            for (iz = 0; iz <= map->node_num[2]; iz++) {
                dz2 = rz - rza;
                dz2m = dz2 - DELTA_FNM;
                dz2p = dz2 + DELTA_FNM;
                dz2 = dz2*dz2;
                dz2m = dz2m*dz2m;
                dz2p = dz2p*dz2p;
                ry = ry0 + y1*dry;
                for (iy = 0; iy <= map->node_num[1]; iy++) {
                    dy2 = ry - rya;
                    dy2m = dy2 - DELTA_FNM;
                    dy2p = dy2 + DELTA_FNM;
                    dy2 = dy2*dy2;
                    dy2m = dy2m*dy2m;
                    dy2p = dy2p*dy2p;
                    rx = rx0 + x1*drx;
                    for (ix = 0; ix <= map->node_num[0]; ix++) {
                        d2 = rx - rxa;
                        nd2[0] = d2 - DELTA_FNM;
                        nd2[1] = d2 + DELTA_FNM;
                        nd2[2] = nd2[3] = nd2[4] = nd2[5] = d2;
                        d2 = d2*d2 + dy2 + dz2;
                        for (kk = 0;kk < 6; kk++) {
                            nd2[kk] = nd2[kk]*nd2[kk];
                        }
                        nd2[0] += dy2 + dz2;
                        nd2[1] += dy2 + dz2;
                        nd2[2] += dy2m + dz2;
                        nd2[3] += dy2p + dz2;
                        nd2[4] += dy2 + dz2m;
                        nd2[5] += dy2 + dz2p;
                        if (d2 <= rc2) {
                            bvs->bvs_map[iz][iy][ix] += ion->ions[ii].occupation_factor*
                                    exp((-sqrtf(d2) + ion->ion_r0[ion_type][ii])/ion->ion_b[ion_type][ii]);
                            // not exectly correct: for each nd2 Rc2 should be checked individually
                            for (kk = 0; kk < 6; kk++) {
                                bvs->bvs_ngh[iz][iy][ix][kk] +=
                                    ion->ions[ii].occupation_factor*
                                        exp((-sqrtf(nd2[kk]) + ion->ion_r0[ion_type][ii])/ion->ion_b[ion_type][ii]);
                            }
                        }
                        rx += drx;
                    }
                    ry += dry;
                }
                rz += drz;
            }
        }
    }
}

void calc_bvs_map_ext(struct BVSData *bvs, struct MapData *map) {
    int ix, iy, iz;

    bvs->bvs_map_ext[0] = 1.0e9;
    bvs->bvs_map_ext[1] = -1.0f;

    for (iz = 0; iz <= map->node_num[2]; iz++) {
        for (iy = 0; iy <= map->node_num[1]; iy++) {
            for (ix = 0; ix <= map->node_num[0]; ix++) {
                if (bvs->bvs_map[iz][iy][ix] > bvs->bvs_map_ext[1]) {
                    bvs->bvs_map_ext[1] = bvs->bvs_map[iz][iy][ix];
                } else if (bvs->bvs_map[iz][iy][ix] < bvs->bvs_map_ext[0]) {
                    bvs->bvs_map_ext[0] = bvs->bvs_map[iz][iy][ix];
                }
            }
        }
    }
}

void calc_bvs_map_da(struct BVSData *bvs, struct MapData *map) {
    int ix, iy, iz;
    float d2, dy2, dz2;

    bvs->bvs_map_ext[0] = 1.0e9;
    bvs->bvs_map_ext[1] = -1.0f;

    for (iz = 0; iz <= map->node_num[2]; iz++) {
        for (iy = 0; iy <= map->node_num[1]; iy++) {
            for (ix = 0; ix <= map->node_num[0]; ix++) {
                d2 = (bvs->bvs_ngh[iz][iy][ix][1] - bvs->bvs_ngh[iz][iy][ix][0])/DELTA_FNM_X2;
                dy2 = (bvs->bvs_ngh[iz][iy][ix][3] - bvs->bvs_ngh[iz][iy][ix][2])/DELTA_FNM_X2;
                dz2 = (bvs->bvs_ngh[iz][iy][ix][5] - bvs->bvs_ngh[iz][iy][ix][4])/DELTA_FNM_X2;
                d2 = d2*d2 + dy2*dy2 + dz2*dz2;
                bvs->bvs_da[iz][iy][ix] = d2;
                if (d2 > bvs->bvs_map_ext[1]) {
                    bvs->bvs_map_ext[1] = d2;
                } else if (d2 < bvs->bvs_map_ext[0]) {
                    bvs->bvs_map_ext[0] = d2;
                }
            }
        }
    }
}

void calc_bvs(int ion_type, struct IonData *ion, struct BVSData *bvs,
              struct MapData *map, struct MapPosData *pos, int verbose) {

    if (verbose) {
        printf("\nMap box origin is at [%.2f,%.2f,%.2f]\n",
           map->box_dim_limit[0][0][0], map->box_dim_limit[0][0][1], map->box_dim_limit[0][0][2]);
        printf("Map grid increments are [%.2f,%.2f,%.2f]\n",
           map->coord_inc[0], map->coord_inc[1], map->coord_inc[2]);
    }

    calc_bvs_map_ngh(ion_type, ion, bvs, map, pos);

    calc_bvs_map_ext(bvs, map);

    if (verbose) {
        printf("BVS map was built with min value = %.2f and max value = %.2f\n",
           bvs->bvs_map_ext[0], bvs->bvs_map_ext[1]);
    }

    calc_bvs_map_da(bvs, map);

    if (verbose) {
        printf("BVS derivatives were calculated with min value = %.2f and max value = %.2f\n",
           bvs->bvs_map_ext[0], bvs->bvs_map_ext[1]);
    }
}

void check_out_of_box_limit(float coord[], struct MapData *map) {
    int i, flag;
    flag = 0;
    for (i = 0; i < 3; i++) {
        if ((coord[i] < map->box_dim_limit[0][0][i]) || (coord[i] > map->box_dim_limit[0][1][i])) {
            if (flag == 0) {
                printf("Coordinate(s) ");
            }
            flag++;            
        }
    }
    if (flag != 0) {
        if (flag == 1) {
            printf("is");
        }
        else {
            printf("are");
        }
        printf(" out of map box limit (%.1f..%.1f;%.1f..%.1f;%.1f..%.1f)\n",
             map->box_dim_limit[0][0][0], map->box_dim_limit[0][1][0],
             map->box_dim_limit[0][0][1], map->box_dim_limit[0][1][1],
             map->box_dim_limit[0][0][2], map->box_dim_limit[0][1][2]);
        printf("BVS value may be calculated incorrect!\n");
    }
}

float calc_bvs_at_coord(float coord[], int it, struct IonData *ion, struct MapPosData *pos) {
    int i, j, pt;
    float bvs, dd, d2, rc2;

    bvs = 0.0f;
    for (i = 0; i < pos->pos_num; i++) {
        pt = pos->positions[i].t - 1;

        d2 = 0.0f;
        for (j = 0; j < 3; j++) {
            dd = pos->positions[i].r[j] - coord[j];
            d2 += dd*dd;
        }

        for (j = pos->ion_type_indices_for_pos[pt].start; j <= pos->ion_type_indices_for_pos[pt].end; j++) {
            rc2 = ion->ion_r_cutoff[it][j];
            if (rc2 <= 0) {
                continue;
            }
            rc2 = rc2*rc2;
            if (d2 <= rc2) {
                bvs += ion->ions[j].occupation_factor*exp((-sqrtf(d2) + ion->ion_r0[it][j])/ion->ion_b[it][j]);
            }
        }
    }
    return bvs;
}

void calc_bvs_cart(int ion_type, float *coord, struct IonData *ion,
                   struct MapData *map, struct MapPosData *pos) {    
    float bvs;        
    check_out_of_box_limit(coord, map);
    bvs = calc_bvs_at_coord(coord, ion_type, ion, pos);
    printf("BVS at (%.1f,%.1f,%.1f) for %s = %.2f\n",
           coord[0], coord[1], coord[2], ion->ions[ion_type].ion_label, bvs);
}

void calc_bvs_pos(int ion_type, int pos_number, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos) {
    float coord[3]; //cartesian coordinates of the point
    coord[0] = pos->positions[pos_number].r[0];
    coord[1] = pos->positions[pos_number].r[1];
    coord[2] = pos->positions[pos_number].r[2];
    calc_bvs_cart(ion_type, coord, ion, map, pos);
}

void calc_bvs_flst(int ion_type, char *filename, struct IonData *ion,
                   struct MapData *map, struct MapPosData *pos) {
    float coord[3]; //cartesian coordinates of the point
    int ip, flag, oob_count = 0;
    float bvs;
    FILE *f_in;
    char poslab[8], oob_s[4];

    if ((f_in = fopen(filename, "r")) == NULL) {
        printf("Cannot open file %s.\n", filename);
        return;
    }

    for(;;) {
        if(fscanf(f_in, "%s%f%f%f", poslab, &coord[0], &coord[1], &coord[2]) != 4) {
            break;
        }

        flag = 0;
        for (ip = 0; ip < 3; ip++) {
            if ((coord[ip] < map->box_dim_limit[0][0][ip]) || (coord[ip] > map->box_dim_limit[0][1][ip])) {
                flag++;
            }
        }

        if (flag!=0) {
            oob_count++;
            sprintf(oob_s, "<%d>", flag);
        } else {
            oob_s[0] = 0.0f;
        }

        bvs = calc_bvs_at_coord(coord, ion_type, ion, pos);
        printf("%s%s at (%.1f,%.1f,%.1f) for %s = %.2f\n",
               poslab, oob_s, coord[0], coord[1], coord[2], ion->ions[ion_type].ion_label, bvs);
    }
    if (oob_count) {
        printf("<n> - number of out-of-box dimentions (%d ent.). BVS for out-of-box points may be wrong!\n", oob_count);
    }
    fclose(f_in);
}

void clear_farray(float *arr, long int num) {
    long int i;
    for (i = 0; i < num; i++) {
        arr[i] = 0.0f;
    }
}

void clear_bvs(struct BVSData *bvs) {
    clear_farray(&bvs->bvs_map[0][0][0], MAX_MAP_DIM*MAX_MAP_DIM*MAX_MAP_DIM); // BV summs
    clear_farray(&bvs->bvs_da[0][0][0], MAX_MAP_DIM*MAX_MAP_DIM*MAX_MAP_DIM);  // Abs for BV derivatives
    clear_farray(&bvs->bvs_ngh[0][0][0][0], MAX_MAP_DIM*MAX_MAP_DIM*MAX_MAP_DIM*6); // BV in node neighbourhood
}

float get_min_grid_size(const struct MapData *map) {
    int i;
    float min_grid_size = 0.0f;
    for (i = 0; i < 3; i++) {
        min_grid_size = fminf(min_grid_size, map->box_dim[i]);
    }
    return min_grid_size/(MAX_MAP_DIM - 1) + 0.01f;
}

void set_grid_size(struct MapData *map, float grid_size) {
    int i;
    for (i = 0; i < 3; i++) {
        map->node_num[i] = (int)(map->box_dim[i]/grid_size);
        map->coord_inc[i] = map->box_dim[i]/map->node_num[i];
    }
}
