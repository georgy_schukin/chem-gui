#ifndef CHEMGUI_READ
#define CHEMGUI_READ

#include "defines.h"

#ifdef __cplusplus
extern "C" {
#endif

void get_input_filename_base(const char *filename, char *base);

void read_data_from_file(const char *filename,
               struct IonData *ion,
               struct MapData *map, struct MapPosData *pos, int verbose);

#ifdef __cplusplus
}
#endif

#endif

