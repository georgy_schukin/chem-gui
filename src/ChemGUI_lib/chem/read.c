#include "read.h"
#include "defines.h"

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

void read_ion_table(FILE *f_in, float data[][MAX_ION_TYPE], int size, int verbose) {
    int i, j;
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            fscanf(f_in, "%f", &data[i][j]);
            if (verbose) {
                printf("%.3f; ", data[i][j]);
            }
        }
        if (verbose) {
            printf("\n");
        }
    }
}

int read_pos_numbers(FILE *f_in, int *pos_nums) {
    char line[256];
    char *token = NULL;
    const char *space = " \t\n";
    int pos_num = 0;

    fgets(line, sizeof(line), f_in);
    token = strtok(line, space);
    while (token != NULL) {
        pos_nums[pos_num] = atoi(token);
        pos_num++;
        token = strtok(NULL, space);
    }
    return pos_num;
}

FILE* open_file_for_read(const char *filename) {
    FILE *f_in;
    if ((f_in = fopen(filename, "r")) == NULL) {
        printf("Cannot open file %s.\n", filename);
        exit(1);
    }
    return f_in;
}

void read_file_type(FILE *f_in) {
    char file_type[80];
    fgets(file_type, 80, f_in);
    if (strstr(file_type, "WXYZ v.3") == NULL) {
        printf("Wrong file type %s. WXYZ v.3 file format is expected.\n", file_type);
        exit(1);
    }
}

void read_title(FILE *f_in, int verbose) {
    char title[80];
    fgets(title, 80, f_in);
    if (verbose) {
        printf("\n Title: %s\n", title);
    }
}

void read_ion_tables(FILE *f_in, struct IonData *ion, int verbose) {
    if (verbose) {
        printf("\nR0 table:\n");
    }
    read_ion_table(f_in, ion->ion_r0, ion->total_ion_num.both, verbose);

    if (verbose) {
        printf("\nb table:\n");
    }
    read_ion_table(f_in, ion->ion_b, ion->total_ion_num.both, verbose);

    if (verbose) {
        printf("\ncutoff R table:\n");
    }
    read_ion_table(f_in, ion->ion_r_cutoff, ion->total_ion_num.both, verbose);
}

void read_ucvert(FILE *f_in, float *vert, const char *coord_str, int verbose) {
    char line[256];
    fgets(line, 256, f_in);
    sscanf(line, "%f%f%f", &vert[0], &vert[1], &vert[2]);
    if (verbose) {
        printf("%s %.4f %.4f %.4f\n", coord_str, vert[0], vert[1], vert[2]);
    }
}

void read_ucverts(FILE *f_in, struct MapData *map, int verbose) {
    read_ucvert(f_in, map->uc_coord[0], "(0,0,0)", verbose);
    read_ucvert(f_in, map->uc_coord[1], "(1,0,0)", verbose);
    read_ucvert(f_in, map->uc_coord[2], "(0,1,0)", verbose);
    read_ucvert(f_in, map->uc_coord[4], "(0,0,1)", verbose);
}

void read_data_from_file(const char *filename, struct IonData *ion,
                         struct MapData *map, struct MapPosData *pos, int verbose) {
    FILE *f_in;
    char str[80];
    int pos_num[MAX_POS_NUM];
    int i, j, p, num_of_pos, num_of_ions, num_of_add_ions;
    float rc_max;
    long int li;
    float r[3];

    f_in = open_file_for_read(filename);

    read_file_type(f_in);
    read_title(f_in, verbose);

    if (verbose) {
        printf("\n Unit cell edges:\n");
    }
    read_ucverts(f_in, map, verbose); // read and define u.c. vertices

    for (i = 0; i < 3; i++) {
        map->uc_coord[3][i] = map->uc_coord[1][i] + map->uc_coord[2][i] - map->uc_coord[0][i];
        map->uc_coord[5][i] = map->uc_coord[1][i] + map->uc_coord[4][i] - map->uc_coord[0][i];
        map->uc_coord[6][i] = map->uc_coord[2][i] + map->uc_coord[4][i] - map->uc_coord[0][i];
        map->uc_coord[7][i] = map->uc_coord[1][i] + map->uc_coord[6][i] - map->uc_coord[0][i];
    }

    // define map box limits
    map->box_dim_limit[0][0][0] = map->box_dim_limit[0][0][1] = map->box_dim_limit[0][0][2] = 10000.0f;
    map->box_dim_limit[0][1][0] = map->box_dim_limit[0][1][1] = map->box_dim_limit[0][1][2] = -10000.0f;
    for (i = 0; i < 8; i++) {
        for (j = 0; j < 3; j++) {
            map->box_dim_limit[0][0][j] = fminf(map->box_dim_limit[0][0][j], map->uc_coord[i][j]);
            map->box_dim_limit[0][1][j] = fmaxf(map->box_dim_limit[0][1][j], map->uc_coord[i][j]);
        }
    }

    // define map box dimensions
    map->box_dim[0] = map->box_dim_limit[0][1][0] - map->box_dim_limit[0][0][0];
    map->box_dim[1] = map->box_dim_limit[0][1][1] - map->box_dim_limit[0][0][1];
    map->box_dim[2] = map->box_dim_limit[0][1][2] - map->box_dim_limit[0][0][2];

    // read basic ion types
    ion->basic_ion_num.anions = ion->basic_ion_num.cations = ion->basic_ion_num.both = 0;
    ion->additional_ion_num.anions = ion->additional_ion_num.cations = ion->additional_ion_num.both = 0;
    ion->total_ion_num.anions = ion->total_ion_num.cations = ion->total_ion_num.both = 0;

    num_of_pos = read_pos_numbers(f_in, pos_num);
    num_of_ions = pos_num[num_of_pos - 1];

    if (verbose) {
        printf("\n Basic ions:\n");
    }
    p=0; j--;
    for (i = 0; i < num_of_ions; i++) {
        if (pos_num[p + 1] == i + 1) {
            p++;
        }
        fscanf(f_in, "%4s%f%d", ion->ions[i].ion_label, &ion->ions[i].occupation_factor, &ion->ions[i].charge);
        ion->ions[i].position_number = p + 1;
        if (ion->ions[i].charge > 0) {
            ion->ions[i].charge_sign = ION_CATION_SIGN;
            ion->basic_ion_num.cations++;
        } else if (ion->ions[i].charge < 0) {
            ion->ions[i].charge_sign = ION_ANION_SIGN;
            ion->basic_ion_num.anions++;
        } else {
            ion->ions[i].charge_sign = ION_BOTH_SIGN;
        }
        ion->basic_ion_num.both++;

        if (verbose) {
            printf("Ion %s, charge %d, charge sign %d, sof %.4f, position number %d\n",
                ion->ions[i].ion_label, ion->ions[i].charge, ion->ions[i].charge_sign,
                   ion->ions[i].occupation_factor, ion->ions[i].position_number);
        }
    }

    if (ion->ions[0].position_number != 1) {
        printf("Wrong position description sequence.\n");
        exit(1);
    }

    //  for(i=0; i<=j; i++) printf("*** %d ",num[i]); printf("\n");
    //  for(i=0;i<num[j];i++) printf(">> iInf.p=%d vs. p=%d\n",iInf[i].p,p); printf("\n");

    p = 1;
    pos->pos_type_num = 0;
    pos->ion_type_indices_for_pos[pos->pos_type_num].start = 0;
    //printf("p2i: 0-");
    for (i = 1; i < num_of_ions; i++) {
        if (verbose) {
            printf(">> iInf.p=%d vs. p=%d\n", ion->ions[i].position_number, p);
        }
        if (ion->ions[i].position_number != p) {
            if (ion->ions[i].position_number - p != 1) {
                printf("...\nWrong position description sequence!\n");
                exit(1);
            }
            pos->ion_type_indices_for_pos[pos->pos_type_num].end = i - 1;
            //printf("%d, ",i-1);
            pos->pos_type_num++;
            p++;
            pos->ion_type_indices_for_pos[pos->pos_type_num].start = i;
            //printf("%d-",i);
        }
    }
    pos->ion_type_indices_for_pos[pos->pos_type_num].end = i - 1;
    //printf("%d.",i-1);
    pos->pos_type_num++;
    //  printf("\n pTNum=%d\n",pTNum);
    //  for (i=0;i<pTNum;i++) printf("****  %d-%d",p2i[i][0],p2i[i][1]); printf("\n");

    // read additional ion types
    if (verbose) {
        printf("\n Additional ions:\n");
    }
    fscanf(f_in, "%d", &num_of_add_ions);
    num_of_ions = ion->basic_ion_num.both + num_of_add_ions;
    for (i = ion->basic_ion_num.both; i < num_of_ions; i++) {
        fscanf(f_in,"%4s%d", ion->ions[i].ion_label, &ion->ions[i].charge);
        ion->ions[i].occupation_factor = 0.0f;
        ion->ions[i].position_number = 0;
        if (ion->ions[i].charge > 0) {
            ion->ions[i].charge_sign = ION_CATION_SIGN;
            ion->additional_ion_num.cations++;
        } else if (ion->ions[i].charge < 0) {
            ion->ions[i].charge_sign = ION_ANION_SIGN;
            ion->additional_ion_num.anions++;
        } else {
            ion->ions[i].charge_sign = ION_BOTH_SIGN;
        }
        ion->additional_ion_num.both++;
        if (verbose) {
            printf("Ion %s, charge %d, charge sign %d, sof %.4f, position number %d\n",
                ion->ions[i].ion_label, ion->ions[i].charge, ion->ions[i].charge_sign,
                   ion->ions[i].occupation_factor, ion->ions[i].position_number);
        }
    }

    ion->total_ion_num.anions = ion->basic_ion_num.anions + ion->additional_ion_num.anions;
    ion->total_ion_num.cations = ion->basic_ion_num.cations + ion->additional_ion_num.cations;
    ion->total_ion_num.both = ion->basic_ion_num.both + ion->additional_ion_num.both;

    if (verbose) {
        printf("\n Total number of ions were read is %d (%d anions, %d cations)\n",
               ion->total_ion_num.both, ion->total_ion_num.anions, ion->total_ion_num.cations);
    }

    if (ion->total_ion_num.both != ion->total_ion_num.anions + ion->total_ion_num.cations) {
        printf("There are some electroneutral atoms were read. Is it OK? [y/n]:");
        scanf("%s", str);
        if ((str[0] == 'n') || (str[0] == 'N')) {
            exit(1);
        }
    }

    read_ion_tables(f_in, ion, verbose);

    rc_max = 0;
    for (i = 0; i < ion->total_ion_num.both; i++) {
        for (j = 0; j < ion->total_ion_num.both; j++) {
            rc_max = fmaxf(rc_max, ion->ion_r_cutoff[i][j]);
        }
    }

    rc_max += 50.0;  //!!!!!!!!!!!!!!!!!!!!!  for debuging !!!!!!!!!!!!!!!!!!!

    // set atom box limits
    if (verbose) {
        printf("Lower map box limits are %.4f, %.4f, %.4f\n", map->box_dim_limit[0][0][0], map->box_dim_limit[0][0][1], map->box_dim_limit[0][0][2]);
        printf("Upper map box limits are %.4f, %.4f, %.4f\n", map->box_dim_limit[0][1][0], map->box_dim_limit[0][1][1], map->box_dim_limit[0][1][2]);
    }
    for (i = 0; i < 3; i++) {
        map->box_dim_limit[1][0][i] = map->box_dim_limit[0][0][i] - rc_max;
        map->box_dim_limit[1][1][i] = map->box_dim_limit[0][1][i] + rc_max;
    }
    if (verbose) {
        printf("Lower atom box limits are %.4f, %.4f, %.4f\n", map->box_dim_limit[1][0][0], map->box_dim_limit[1][0][1], map->box_dim_limit[1][0][2]);
        printf("Upper atom box limits are %.4f, %.4f, %.4f\n", map->box_dim_limit[1][1][0], map->box_dim_limit[1][1][1], map->box_dim_limit[1][1][2]);
    }

    // read positions matching to the limits
    fscanf(f_in, "%li", &li);
    if (verbose) {
        printf("\n Reading position list (%li entities)\n", li);
    }
    pos->pos_num = 0;
    while (li > 0) {
        fscanf(f_in, "%d%f%f%f", &i, &r[0], &r[1], &r[2]);
        li--;
        p = 1;
        for (j = 0; j < 3; j++) {
        if ((r[j] < map->box_dim_limit[1][0][j]) || (r[j] > map->box_dim_limit[1][1][j])) {
            p = 0;
            break;
        }
    }
    if (p == 1) { // add new position
        pos->positions[pos->pos_num].t = i;
        pos->positions[pos->pos_num].r[0] = r[0];
        pos->positions[pos->pos_num].r[1] = r[1];
        pos->positions[pos->pos_num].r[2] = r[2];
        pos->pos_num++;
        if (pos->pos_num >= MAX_POS_NUM) {
            printf("Number of positions in atom box exceeds the limit (%d).\n", MAX_POS_NUM);
            exit(1);
        }
        if (pos->pos_num < 5) {
            if (verbose) {
                printf(" %d   %.4f, %.4f, %.4f\n", i, r[0], r[1], r[2]);
            }
        }
    }
    //else printf("- %d   %.4f, %.4f, %.4f\n",i,r[0],r[1],r[2]);
    }
    if (verbose) {
        printf("...\n %li positions are in the atom box limits.\n", pos->pos_num);
    }

    // check END
    fgets(str, 80, f_in);
    fgets(str, 80, f_in);
    if (strstr(str, "END") == NULL) {
        printf("String =>%s<= instead =>END<= was found.\n", str);
        printf("Wrong file ending. \n");
        exit(1);
    }

    fclose(f_in);
}

void get_input_filename_base(const char *filename, char *base) {
    int len;
    const char *extension = ".wxyz";
    if (strstr(filename, extension) != NULL) {
        len = strlen(filename) - strlen(extension);
    } else {
        len = strlen(filename);
    }
    strncpy(base, filename, len);
    base[len] = '\0';
}

