#include "save.h"
#include "defines.h"

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

void get_cub_filename(const char *base, const char *tag, const char *ion_label, char *filename) {
    if (strlen(tag) > 0) {
        sprintf(filename, "%s_%s_%s.cub", base, tag, ion_label);
    } else {
        sprintf(filename, "%s_%s.cub", base, ion_label);
    }
}

void write_map_origin(FILE *f_out, int p_num, struct MapData *map) {
    fprintf(f_out, "%5d%12.6f%12.6f%12.6f\n",
                 8 + p_num, map->box_dim_limit[0][0][0], map->box_dim_limit[0][0][1], map->box_dim_limit[0][0][2]); // origin
    fprintf(f_out, "%5d%12.6f%12.6f%12.6f\n", map->node_num[0] + 1, map->coord_inc[0], 0.0f, 0.0f); // dx
    fprintf(f_out, "%5d%12.6f%12.6f%12.6f\n", map->node_num[1] + 1, 0.0f, map->coord_inc[1], 0.0f); // dy
    fprintf(f_out, "%5d%12.6f%12.6f%12.6f\n", map->node_num[2] + 1, 0.0f, 0.0f, map->coord_inc[2]); // dz
}

void write_map_ucvert(FILE *f_out, struct MapData *map) {
    int ix;
    for (ix = 0; ix < 8; ix++) {
        fprintf(f_out,"1  1.0  %12.5f  %12.5f  %12.5f\n", map->uc_coord[ix][0], map->uc_coord[ix][1], map->uc_coord[ix][2]);
    }
}

void write_pos_plst(FILE *f_out, struct MapPosData *pos) {
    int ix;
    for (ix = 0; ix < pos->pos_num; ix++) {
        fprintf(f_out, "%d  1.0  %12.5f  %12.5f  %12.5f\n",
             pos->positions[ix].t + 1, pos->positions[ix].r[0], pos->positions[ix].r[1], pos->positions[ix].r[2]);
    }
}

void write_cub_header(FILE *f_out, struct MapData *map, struct MapPosData *pos) {
    write_map_origin(f_out, pos->pos_num, map);
    write_map_ucvert(f_out, map);
    write_pos_plst(f_out, pos);
}

FILE* open_file_for_write(const char *filename) {
    FILE *out;
    if ((out = fopen(filename, "w")) == NULL) {
        fprintf(stderr, "Error: cannot create file %s\n", filename);
        return 0;
    }
    return out;
}

void save_da_cube(const char *filename, int ion_type,
                  struct IonData *ion, struct BVSData *bvs,
                  struct MapData *map, struct MapPosData *pos) {
    int ix, iy, iz;    

    FILE *f_out = open_file_for_write(filename);
    if (!f_out) {
        return;
    }

    //fprintf(f_out, "Abs. values for the derivatives of BVS generated for %s.wxyz anions data\n", filename);
    fprintf(f_out, "Abs. values for the derivatives of BVS generated for anions data\n");
    fprintf(f_out, "  by BVS program, ver.3.0. Min=%.2f, max=%.2f\n", bvs->bvs_map_ext[0], bvs->bvs_map_ext[1]);

    write_cub_header(f_out, map, pos);

    for (ix = 0; ix <= map->node_num[0]; ix++) {
        for (iy = 0; iy <= map->node_num[1]; iy++) {
            for (iz = 0; iz <= map->node_num[2]; iz++) {
                fprintf(f_out, "%g ", bvs->bvs_da[iz][iy][ix]);
                if (iz % 6 == 5) fprintf(f_out,"\n");
            }
            fprintf(f_out,"\n");
        }
    }
    fprintf(f_out, "\n");
    fclose(f_out);
}

void save_bvs_cube(const char *filename, int ion_type,
                   struct IonData *ion, struct BVSData *bvs,
                   struct MapData *map, struct MapPosData *pos) {
    int ix, iy, iz;

    FILE *f_out = open_file_for_write(filename);
    if (!f_out) {
        return;
    }

    //fprintf(f_out, "Bond valence summ generated for %s.wxyz anions data\n", filename);
    fprintf(f_out, "Bond valence summ generated for anions data\n");
    fprintf(f_out, "  by BVS program, ver.3.0. Min=%.2f, max=%.2f\n", bvs->bvs_map_ext[0], bvs->bvs_map_ext[1]);

    write_cub_header(f_out, map, pos);

    for (ix = 0; ix <= map->node_num[0]; ix++) {
        for (iy = 0;iy <= map->node_num[1]; iy++) {
            for (iz = 0; iz <= map->node_num[2]; iz++) {
                fprintf(f_out, "%g ", bvs->bvs_map[iz][iy][ix]);
                if (iz % 6 == 5) fprintf(f_out,"\n");
            }
            fprintf(f_out,"\n");
        }
    }
    fprintf(f_out, "\n");
    fclose(f_out);
}

void save_gii_cube(const char *filename, int ion_type,
                   struct IonData *ion, struct BVSData *bvs,
                   struct MapData *map, struct MapPosData *pos) {
    int ix, iy, iz;    
    int charge = ion->ions[ion_type].charge;

    FILE *f_out = open_file_for_write(filename);
    if (!f_out) {
        return;
    }

    //fprintf(f_out, "GII for %s.wxyz anions data\n", filename);
    fprintf(f_out, "GII for anions data\n");
    fprintf(f_out, "  for ions with charge %d.\n", charge);

    write_cub_header(f_out, map, pos);

    for (ix = 0; ix <= map->node_num[0]; ix++) {
        for (iy = 0; iy <= map->node_num[1]; iy++) {
            for (iz = 0; iz <= map->node_num[2]; iz++) {
                fprintf(f_out, "%g ",
                        (bvs->bvs_map[iz][iy][ix] - abs(charge))*
                        (bvs->bvs_map[iz][iy][ix] - abs(charge)));
                if (iz % 6 == 5) fprintf(f_out,"\n");
            }
            fprintf(f_out,"\n");
        }
    }
    fprintf(f_out, "\n");
    fclose(f_out);
}
