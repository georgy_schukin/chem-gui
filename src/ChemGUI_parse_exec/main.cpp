#include "chemgui_parse.h"
#include "parse/structure_util.h"

#include <iostream>
#include <string>
#include <memory>
#include <fstream>

void write_result(const char *filename, StructureData *data) {
    std::ofstream out(filename);
    // Insert data processing to *.wxyz.
    out << *data;
    out.close();
}

int main(int argc, char *argv[]) {
    if (argc <= 2) {
        std::cerr << "Usage: " << argv[0] << " input_file output_file" << std::endl;
        return -1;
    }

    const std::string input_file(argv[1]);
    const std::string output_file(argv[2]);

    std::unique_ptr<StructureData> data_ptr(new StructureData());
    StructureData *data = data_ptr.get();

    parse_shelx(input_file.c_str(), data);
    write_result(output_file.c_str(), data);    

    return 0;
}
