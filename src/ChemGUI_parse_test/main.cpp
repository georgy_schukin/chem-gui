#include "chemgui_parse.h"
#include "chemgui_table_gen.h"
#include "parse/structure_util.h"
#include "cryst_math.h"

#include <iostream>
#include <memory>

void print_table(StructureData *data, TableGenType type) {
    float table[1024];
    const int sf_num = data->scatter_factors_count;

    generate_ion_table(type, data->scatter_factors, sf_num, table, 1024);

    for (int i = 0; i < sf_num; i++) {
        for (int j = 0; j < sf_num; j++) {
            std::cout << table[i*sf_num + j] << " ";
        }
        std::cout << "\n";
    }
}

int main(int argc, char *argv[]) {
    try {        
        std::unique_ptr<StructureData> data_ptr(new StructureData());
        StructureData *data = data_ptr.get();
        parse_shelx("../data/test2.ins", data);
        std::cout << *(data) << std::endl;


        // initialisation of fields of (StruData)data, based on data were read from ins-file
        init_bravais_translation_vectors(data); // *** it is already initialized! but it should contais zero vector!!!

        init_unit_cell_matrix(data);
        std::cout << "\n---------------  UCM  -------------------\n";
        std::cout << "( " << data->unit_cell_matrix.data[0][0] << " , " << data->unit_cell_matrix.data[0][1] << " , " << data->unit_cell_matrix.data[0][2] << ")" << std::endl;
        std::cout << "( " << data->unit_cell_matrix.data[1][0] << " , " << data->unit_cell_matrix.data[1][1] << " , " << data->unit_cell_matrix.data[1][2] << ")" << std::endl;
        std::cout << "( " << data->unit_cell_matrix.data[2][0] << " , " << data->unit_cell_matrix.data[2][1] << " , " << data->unit_cell_matrix.data[2][2] << ")" << std::endl;

        extend_shelx_symm_operator_list(data);
        // !!! is_it_group(data);
        std::cout << "\n-----------------------------------------\n";
        std::cout << *(data) << std::endl;

        init_shelx_multiplicity_occupancy(data);

        init_bvs_types(data);
        std::cout << "\n------------- BVS Types ----------------\n";
        std::cout << data->bvs_types_count << std::endl;
        for (int i=0; i<data->bvs_types_count; i++)
            std::cout << "(" << data->bvs_types[i].oxidation_state <<
                         "; " << data->bvs_types[i].element_number <<
                         "; " << data->bvs_types[i].site_occupation_factor << "), ";
        std::cout << std::endl;


        // generation of atomic model
        fill_unit_cell(data);
        // !!! extend_1x1_to_3x3_unit_cell(&data);


        // *** TABLES should be generated using data.bvs_types data, not SFAC ones.
        std::cout << "\nR0 table:" << std::endl;
        print_table(data, GEN_R0_TABLE);

        std::cout << "\nB table:" << std::endl;
        print_table(data, GEN_B_TABLE);

        std::cout << "\nRc table:" << std::endl;
        print_table(data, GEN_RC_TABLE);
    }
    catch (const std::exception &e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }
}
