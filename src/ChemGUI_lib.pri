include(ChemGUI.pri)

INCLUDEPATH += "$${LIB_INCLUDEDIR}"

LIBS += -L\"$${LIBDIR}\" -l$${CHEM_LIB}

win32-g++|unix {
    PRE_TARGETDEPS += $${LIBDIR}/lib$${CHEM_LIB}.a
}
else:win32 {
    PRE_TARGETDEPS += $${LIBDIR}/$${CHEM_LIB}.lib
}
