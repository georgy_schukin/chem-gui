#include "app.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

void do_calc_bvs_cart(int ion_type, struct IonData *ion,
                   struct MapData *map, struct MapPosData *pos) {
    float coord[3]; //cartesian coordinates of the point
    printf("Enter cartesian coordinates for BVS calculation: ");
    scanf("%f%f%f", &coord[0], &coord[1], &coord[2]);
    calc_bvs_cart(ion_type, coord, ion, map, pos);
}

void do_calc_bvs_pos(int ion_type, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos) {
    int pos_number;
    printf("Enter position number for BVS calculation: ");
    scanf("%d", &pos_number);
    if ((pos_number < 1) || (pos_number > pos->pos_num)) {
        printf("Number of position should be in 1..%d range!\n", pos->pos_num);
        return;
    }
    pos_number--;
    calc_bvs_pos(ion_type, pos_number, ion, map, pos);
}

void do_calc_bvs_flst(int ion_type, struct IonData *ion,
                   struct MapData *map, struct MapPosData *pos) {
    char filename[256];
    printf("Enter file name with a list of cartesian coordinates for BVS calculation: ");
    scanf("%s", filename);
    calc_bvs_flst(ion_type, filename, ion, map, pos);
}

void print_grid(struct MapData *map) {
    int i;
    printf("Map grid of ");
    for (i = 0; i < 3; i++) {
        printf("%d", map->node_num[i]);
        if (i < 2) {
            printf("x");
        }
    }
    printf(" dimentions will be used.\n");
}

float get_grid_size(float min_grid_size) {
    float grid_size;
    printf("\nEnter BV map grid size (greater then %.2f): ", min_grid_size);
    scanf("%f", &grid_size);
    if (grid_size < min_grid_size) {
        printf("The grid size is too small. Enlarge MAPMAXDIM constant to use %.2f grid!\n", grid_size);
        exit(1);
    }
    return grid_size;
}

int choose_calc_mode(int *choice, struct IonData *ion, int num_of_ions) {
    int i;
    int num_of_tries = 0;

    printf("Possible ion types:\n");
    for (i = 0; i < num_of_ions; i++) {
        printf("\t(%d) %s\n", i+1, ion->ions[i].ion_label);
    }

    printf("\nSelect index of test ion (1-%d),\n", num_of_ions);
    printf("%d for single-point BVS calculation,\n", num_of_ions + 1);
    printf("%d for BVS calculation in atom position,\n", num_of_ions + 2);
    printf("%d for file listed positions BVS calculation,\n", num_of_ions + 3);
    printf("0 for exit\n");
    do {
        scanf("%d", choice);
        if (*choice == 0) {
            printf("BV map calculation was terminated.\n");
            return -1;
        }
        else if ((*choice < 0) || (*choice > num_of_ions + 3)) {
            printf("Inacceptable index! Try again: ");
            num_of_tries++;
        }
        else {
            break;
        }
    } while (num_of_tries < 3);
    if (num_of_tries == 3) {
        printf("Inacceptable index! Program is terminated.\n");
        return -1;
    }    
    return 0;
}

int get_ion_type(int num_of_ions) {
    int ion_type;
    printf("Enter type of ion (1-%d): ", num_of_ions);
    scanf("%d", &ion_type);
    if ((ion_type < 1) || (ion_type > num_of_ions)) {
        printf("Improper ion type number!\n");
        return -1;
    }
    return ion_type - 1;
}

void get_input_filename(char *filename) {
    printf("Enter wxyz filename: ");
    scanf("%s", filename);
    if (strstr(filename, ".wxyz") == NULL) {
        strcat(filename, ".wxyz");
    }
}

int run_app(const char *filename) {
    int choice, ion_type, num_of_ions;
    const char *ion_label;
    float grid_size;

    struct IonData *ion;
    struct BVSData *bvs;
    struct MapData *map;
    struct MapPosData *pos;

    char fname_base[80];
    char bvs_filename[80], gii_filename[80], da_filename[80];    

    printf("Use file %s\n", filename);

    ion = (struct IonData*)malloc(sizeof(struct IonData));
    bvs = (struct BVSData*)malloc(sizeof(struct BVSData));
    map = (struct MapData*)malloc(sizeof(struct MapData));
    pos = (struct MapPosData*)malloc(sizeof(struct MapPosData));

    get_input_filename_base(filename, fname_base);

    read_data_from_file(filename, ion, map, pos, 1);

    grid_size = get_grid_size(get_min_grid_size(map));
    set_grid_size(map, grid_size);
    print_grid(map);

    choice = 0;
    num_of_ions = ion->total_ion_num.both;
    while (choose_calc_mode(&choice, ion, num_of_ions) != -1) {
        if (choice > num_of_ions) { // special calculations
            ion_type = get_ion_type(num_of_ions);
            if (ion_type == -1) {
                continue;
            }
            if (choice == num_of_ions + 1) {
                do_calc_bvs_cart(ion_type, ion, map, pos);
            } else if (choice == num_of_ions + 2) {
                do_calc_bvs_pos(ion_type, ion, map, pos);
            } else if (choice == num_of_ions + 3) {
                do_calc_bvs_flst(ion_type, ion, map, pos);
            }            
        } else {
            printf("\nProcessing BV map with %dth test ion.\n", choice);

            ion_type = choice - 1;
            ion_label = ion->ions[ion_type].ion_label;

            clear_bvs(bvs);
            calc_bvs(ion_type, ion, bvs, map, pos, 1);  // Calculate BVS for i-th ion type

            get_cub_filename(fname_base, "", ion_label, bvs_filename);
            get_cub_filename(fname_base, "GII", ion_label, gii_filename);
            get_cub_filename(fname_base, "DA", ion_label, da_filename);

            save_bvs_cube(bvs_filename, ion_type, ion, bvs, map, pos);
            save_gii_cube(gii_filename, ion_type, ion, bvs, map, pos);
            save_da_cube(da_filename, ion_type, ion, bvs, map, pos);
        }
    }

    free(ion);
    free(bvs);
    free(map);
    free(pos);

    return 0;
}


