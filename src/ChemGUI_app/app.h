#ifndef CHEMGUI_APP
#define CHEMGUI_APP

#include "chemgui.h"

#ifdef __cplusplus
extern "C" {
#endif

int run_app(const char *filename);

void do_calc_bvs_cart(int ion_type, struct IonData *ion,
                   struct MapData *map, struct MapPosData *pos);

void do_calc_bvs_pos(int ion_type, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos);

void do_calc_bvs_flst(int ion_type, struct IonData *ion,
                  struct MapData *map, struct MapPosData *pos);

void get_input_filename(char *filename);

#ifdef __cplusplus
}
#endif

#endif
