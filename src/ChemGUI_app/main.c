#include "app.h"

#include <stdio.h>

int main(int argc, char **argv) {

    char filename[80];

    printf("*************************************************\n");
    printf("*                                               *\n");
    printf("*    Bond valence summ maps calculation         *\n");
    printf("*     ver.3.1      27.10.2014                   *\n");
    printf("*     Komarov V.Yu. NIIC SB RAS                 *\n");
    printf("*                                               *\n");
    printf("*************************************************\n");
    printf("\n\n\n\n\n\n\n");

    get_input_filename(filename);
    run_app(filename);

    return 0;
}

