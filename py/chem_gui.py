from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *
import ctypes
import threading

def run_app(filename):
    dll = ctypes.CDLL("../src/ChemGUI.dll")
    run_func = dll.run_app
    run_func.restype = ctypes.c_int
    run_func.argtypes = [ctypes.c_char_p]
    c_filename = ctypes.create_string_buffer(filename.encode("utf-8"))
    run_func(c_filename)

def open_file():
    filename = askopenfilename(defaultextension=".wxyz",
                            filetypes=[("WXYZ", ".wxyz"), ("All files", "*.*")],
                            title="Open data file")
    if len(filename) > 0:
        thread = threading.Thread(target=run_app, args=(filename,))
        thread.daemon = True
        thread.start()

def about():
    about_text = "ChemGUI v.1.0\nDeveloped by Georgy Schukin"
    showinfo("About", about_text)

def select_ion_index():
    pass

def single_point_bvs():
    pass

def atom_bvs():
    pass

def file_bvs():
    pass

def main():
    root = Tk()
    root.title("ChemGUI")

    text = Text(root)
    text.grid(row=0)

    menu = Menu(root)
    root.config(menu=menu)

    file_menu = Menu(menu, tearoff=False)
    file_menu.add_command(label="Open...", command=open_file)
    file_menu.add_command(label="Exit", command=lambda: root.destroy())

    actions_menu=Menu(menu, tearoff=False)
    actions_menu.add_command(label="Select ion index...", command=select_ion_index)
    actions_menu.add_command(label="Calculate single-point BVS", command=single_point_bvs)
    actions_menu.add_command(label="Calculate BVS in atom position", command=atom_bvs)
    actions_menu.add_command(label="Calculate BVS for file listed positions", command=file_bvs)

    menu.add_cascade(menu=file_menu, label="File", underline=0)
    menu.add_cascade(menu=actions_menu, label="Actions", underline=0)
    menu.add_command(label="About", command=about)
    
    root.mainloop()

if __name__ == '__main__':
    main()
