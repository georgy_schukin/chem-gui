from compute.library import Library
from compute.types import *
from compute.funcs import set_grid

class MyStruct(ctypes.Structure):
    _fields_ = [("a", ctypes.c_int),
                ("b", ctypes.c_char)]

def main():
    lib = Library("ChemGUI", "../src")
    func = lib.get_func("test2", ctypes.c_int, [ctypes.POINTER(MyStruct)])
    s = MyStruct()
    func(s)
    print(s.a, s.b)

def test2():
    md = MapData()
    print("Setting...")
    set_grid(md)
    print("Done")
    print(md)

if __name__ == "__main__":
    main()
    test2()
