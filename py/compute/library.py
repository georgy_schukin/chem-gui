import ctypes
import os

class Library:
    def __init__(self, name, directory=""):
        self.name = name
        self.directory = directory
        win_lib = os.path.join(directory, name + ".dll")
        linux_lib = os.path.join(directory, "lib" + name + ".so")
        try:
            if os.path.exists(win_lib):
                self.dll = ctypes.CDLL(win_lib)
            elif os.path.exists(linux_lib):
                self.dll = ctypes.CDLL(linux_lib)
        except OSError:
            print("Error: " + str(OSError.strerror))
            self.dll = None

    def get_func(self, func_name, result_type, arg_types):
        func = getattr(self.dll, func_name)
        func.restype = result_type
        func.argtypes = arg_types
        return func
