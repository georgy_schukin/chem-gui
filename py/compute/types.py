from .consts import *
import ctypes


class IonDescrType(ctypes.Structure):
    _fields_ = [
        ("charge", ctypes.c_int),
        ("charge_sign", ctypes.c_int),
        ("position_number", ctypes.c_int),
        ("float", ctypes.c_float),
        ("ion_label", ctypes.c_char * 5)
    ]


class PDescrType(ctypes.Structure):
    _fields_ = [
        ("t", ctypes.c_int),
        ("r", ctypes.c_float * 3)
    ]


class IonNumber(ctypes.Structure):
    _fields_ = [
        ("anions", ctypes.c_int),
        ("cations", ctypes.c_int),
        ("both", ctypes.c_int)
    ]


class MapData(ctypes.Structure):
    _fields_ = [
        ("UCVert", (ctypes.c_float * 3) * 8),
        ("MapVec", ctypes.c_float * 3),
        ("MapInc", ctypes.c_float * 3),
        ("MapNNum", ctypes.c_int * 3),
        ("ITNum", IonNumber * 3),
        ("BoxDimLim", ((ctypes.c_float * 3) * 2) * 2)
    ]


class MapPosData(ctypes.Structure):
    _fields_ = [
        ("pLst", PDescrType * MAX_P_NUM),
        ("p2i", (ctypes.c_int * 2) * (MAX_P_NUM + 1)),
        ("pTNum", ctypes.c_int),
        ("pNum", ctypes.c_longlong)
    ]

class IonData(ctypes.Structure):
    _fields_ = [
        ("iInf", IonDescrType * ION_MAX_TYPE),
        ("iR0", (ctypes.c_float * ION_MAX_TYPE) * ION_MAX_TYPE),
        ("i_b", (ctypes.c_float * ION_MAX_TYPE) * ION_MAX_TYPE),
        ("iRc", (ctypes.c_float * ION_MAX_TYPE) * ION_MAX_TYPE)
    ]

class BVSData(ctypes.Structure):
    _fields_ = [
        ("BVS_Map", ((ctypes.c_float * MAP_MAX_DIM) * MAP_MAX_DIM) * MAP_MAX_DIM),
        ("BVS_DA", ((ctypes.c_float * MAP_MAX_DIM) * MAP_MAX_DIM) * MAP_MAX_DIM),
        ("BVS_Ngh", ((ctypes.c_float * MAP_MAX_DIM) * MAP_MAX_DIM) * MAP_MAX_DIM),
        ("BVS_MapExt", ctypes.c_float * 2)
    ]
