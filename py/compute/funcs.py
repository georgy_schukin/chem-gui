from .library import Library
from .types import *
import ctypes

_library = Library("ChemGUI", "../src")

set_grid = _library.get_func("set_grid", None, [ctypes.POINTER(MapData)])

if __name__ == "__main__":
    pass
