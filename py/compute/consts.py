MAX_P_NUM = 32768
MAP_MAX_DIM = 256
ION_MAX_TYPE = 32
DELTA_FNM = 0.001  # delta (in Angstroms) for numerical differentiation
DELTA_FNM_X2 = 0.002

ION_ANION_SIGN = -1
ION_CATION_SIGN = 1
ION_BOTH_SIGN = 0

ION_ANIONS_NUM = 0
ION_CATIONS_NUM = 1
ION_BOTH_NUM = 2

if __name__ == "__main__":
    pass
