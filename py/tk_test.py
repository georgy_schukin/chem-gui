from tkinter import *
from tkinter.filedialog import *
from tkinter.messagebox import *

def open_file():
    filename = askopenfilename(defaultextension=".wxyz",
                            filetypes=[("WXYZ", ".wxyz"), ("All files", "*.*")],
                            title="Open file")
    if len(filename) > 0:
        showinfo("File", filename)

def main():
    root = Tk()

    button = Button(root, text="Hello")
    button.bind("<Button-1>", lambda event: print("Hello world!"))
    button.pack()

    label = Label(root, text="Hello world!")
    label.pack()

    entry = Entry(root, width=30)
    entry.pack()

    text = Text(root, width=30, height=4, wrap=WORD)
    text.pack()

    ins_button = Button(root, text="Insert")
    ins_button.bind("<Button-1>", lambda event: text.insert(END, entry.get()))
    ins_button.pack()

    clear_button = Button(root, text="Clear")
    clear_button.bind("<Button-1>", lambda event: text.delete(1.0, END))
    clear_button.pack()

    radio_var = IntVar()
    radio_var.set(1)
    radio1 = Radiobutton(root, text="First", variable=radio_var, value=1)
    radio2 = Radiobutton(root, text="Second", variable=radio_var, value=2)
    radio1.pack()
    radio2.pack()

    check_var1 = IntVar()
    check_var2 = IntVar()
    check1 = Checkbutton(root, text="Boost", variable=check_var1, onvalue=1, offvalue=0)
    check2 = Checkbutton(root, text="Drive", variable=check_var2, onvalue=1, offvalue=0)
    check1.pack()
    check2.pack()

    items = ["Boom", "Box", "Screen", "Deck"]
    lst = Listbox(root, selectmode=SINGLE, height=4)
    for item in items:
        lst.insert(END, item)
    lst.pack()

    frame = Frame(root, width=100, height=20, bg="blue", bd=10)
    frame.pack()

    scale = Scale(frame, orient=HORIZONTAL, length=180, from_=0, to=10, tickinterval=2, resolution=1)
    scale.pack()

    scroll = Scrollbar(root, orient=HORIZONTAL, command=text.yview)
    text.configure(yscrollcommand=scroll.set)
    scroll.pack()

    menu = Menu(root)
    root.config(menu=menu)

    file_menu = Menu(menu)
    menu.add_cascade(menu=file_menu, label="File")
    file_menu.add_command(label="Open...", command=open_file)
    file_menu.add_command(label="Exit", command=lambda: root.destroy())

    root.mainloop()

if __name__ == '__main__':
    main()
